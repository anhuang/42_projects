<?php
class UnholyFactory{
	private $all_type = array();
	function absorb($something) {
		if ($something instanceof Fighter) {
			if (!in_array($something->getType(), $this->all_type)) {
				print "(Factory absorbed a fighter of type " . $something->getType() . ")\n";
				array_push($this->all_type, $something->getType());
			} else {
				print "(Factory already absorbed a fighter of type " . $something->getType() . ")\n";
			}
		} else {
			print "(Factory can't absorb this, it's not a fighter)\n";
		}
	}
	function fabricate($type) {
		if (in_array($type, $this->all_type)) {
			print "(Factory fabricates a fighter of type " . $type . ")\n";
			$ret = ucfirst(preg_replace("/ /", "", $type));
			return new $ret;
		} else
			print "(Factory hasn't absorbed any fighter of type " . $type . ")\n";
	}
}

?>
