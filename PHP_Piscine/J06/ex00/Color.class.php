<?php
class Color
{
	public $red;
	public $green;
	public $blue;
	public static $verbose = False;
	public function __construct(array $kwargs) {
		if (array_key_exists ( 'rgb' , $kwargs )) {
			$this->red = ($kwargs['rgb'] & 0xFF0000) >> 16;
			$this->green = ($kwargs['rgb'] & 0xFF00) >> 8;
			$this->blue = $kwargs['rgb'] & 0xFF;
		}
		else if (array_key_exists ( 'red' , $kwargs )
				&& array_key_exists ( 'green' , $kwargs )
				&& array_key_exists ( 'blue' , $kwargs )) {
			$this->red = $kwargs['red'];
			$this->green = $kwargs['green'];
			$this->blue = $kwargs['blue'];
		}
		if (self::$verbose === True)
			print("Color( red: ".sprintf("%3d", $this->red).", green: ".sprintf("%3d", $this->green).", blue: ".sprintf("%3d", $this->blue)." ) constructed.".PHP_EOL);
	}
	public function __destruct() {
		if (self::$verbose === True)
			print("Color( red: ".sprintf("%3d", $this->red).", green: ".sprintf("%3d", $this->green).", blue: ".sprintf("%3d", $this->blue)." ) destructed.".PHP_EOL);
	}
	function __toString() {
			return ("Color( red: ".sprintf("%3d", $this->red).", green: ".sprintf("%3d", $this->green).", blue: ".sprintf("%3d", $this->blue)." )");
	}
	static function doc() {
		return (file_get_contents('Color.doc.txt'));
	}
	function add($color) {
		return (new Color(array('red' => $this->red + $color->red, 'green' => $this->green + $color->green, 'blue' => $this->blue + $color->blue )));
	}
	function sub($color) {
		return (new Color(array('red' => $this->red - $color->red, 'green' => $this->green - $color->green, 'blue' => $this->blue - $color->blue )));
	}
	function mult($color) {
		return (new Color(array('red' => $this->red * $color, 'green' => $this->green * $color, 'blue' => $this->blue * $color )));
	}
}
?>
