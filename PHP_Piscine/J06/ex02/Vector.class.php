<?php
require_once "../ex01/Vertex.class.php";

class Vector {
	private $_x;
	private $_y;
	private $_z;
	private $_w = 0;
	public static $verbose = False;
	function getX() {
		return ($this->_x);
	}
	function getY() {
		return ($this->_y);
	}
	function getZ() {
		return ($this->_z);
	}
	function getW() {
		return ($this->_w);
	}
	function __construct($kwargs) {
		if (!isset($kwargs['orig'])) {
			$kwargs['orig'] = new Vertex(array( 'x' => 0.0, 'y' => 0.0, 'z' => 0.0 ));
		}
		if (array_key_exists('dest', $kwargs)) {
			$this->_x = $kwargs['dest']->getX() - $kwargs['orig']->getX();
			$this->_y = $kwargs['dest']->getY() - $kwargs['orig']->getY();
			$this->_z = $kwargs['dest']->getZ() - $kwargs['orig']->getZ();
		}
		if (self::$verbose === True)
			print("Vector( x:".sprintf("%.2f", $this->_x).", y:".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).$this->_Color." ) constructed".PHP_EOL);

	}
	public function __destruct() {
		if (self::$verbose === True)
			print("Vector( x:".sprintf("%.2f", $this->_x).", y:".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).$this->_Color." ) destructed".PHP_EOL);
	}
	function __toString() {
		if (self::$verbose === True)
			return ("Vector( x:".sprintf("%.2f", $this->_x).", y:".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).$this->_Color." )");
		else
			return ("Vector( x:".sprintf("%.2f", $this->_x).", y:".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w)." )");
	}
	static function doc() {
		return (file_get_contents('../ex02/Vector.doc.txt'));
	}
	function add($rhs) {
		$dest = new Vertex( array( 'x' => $this->_x + $rhs->getX(), 'y' => $this->_y + $rhs->getY(), 'z' => $this->_z + $rhs->getZ()));
		return new Vector(array('dest' => $dest));
		}
	function sub($rhs) {
		$dest = new Vertex( array( 'x' => $this->_x - $rhs->getX(), 'y' => $this->_y - $rhs->getY(), 'z' => $this->_z - $rhs->getZ()));
		return new Vector(array('dest' => $dest));
	}
	function magnitude() {
		return sqrt($this->_x*$this->_x + $this->_y*$this->_y + $this->_z*$this->_z);
	}
	function normalize() {
		$dest = new Vertex( array( 'x' => $this->_x / $this->magnitude(), 'y' => $this->_y / $this->magnitude(), 'z' => $this->_z / $this->magnitude()));
		return new Vector(array('dest' => $dest));
	}
	function opposite() {
		$dest = new Vertex( array( 'x' => $this->_x * -1, 'y' => $this->_y * -1, 'z' => $this->_z * -1));
		return new Vector(array('dest' => $dest));
	}
	function scalarProduct($k) {
		$dest = new Vertex( array( 'x' => $this->_x * $k, 'y' => $this->_y * $k, 'z' => $this->_z * $k));
		return new Vector(array('dest' => $dest));
	}
	function dotProduct($rhs) {
		$dest = new Vertex( array( 'x' => $this->_x * $rhs->getX(), 'y' => $this->_y * $rhs->getY(), 'z' => $this->_z * $rhs->getZ()));
		return $dest->getX() + $dest->getY() + $dest->getZ();
	}
	function crossProduct($rhs) {
		$dest = new Vertex( array( 'x' => $this->_y * $rhs->getZ() - $this->_z * $rhs->getY(), 'y' => $this->_z * $rhs->getX() - $this->_x * $rhs->getZ(), 'z' => $this->_x * $rhs->getY() - $this->_y *$rhs->getX()));
		return new Vector(array('dest' => $dest));
	}
	function cos($rhs) {
		return (($this->_x * $rhs->_x + $this->_y * $rhs->_y + $this->_z * $rhs->_z) / ($this->magnitude() * $rhs->magnitude()));
	}
}

?>
