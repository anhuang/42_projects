<?php
require_once "../ex00/Color.class.php";

class Vertex {
	private $_x;
	private $_y;
	private $_z;
	private $_w = 1.0;
	private $_Color;
	public static $verbose = False;
	public function __construct(array $kwargs) {
		$this->_x = floatval($kwargs['x']);
		$this->_y = floatval($kwargs['y']);
		$this->_z = floatval($kwargs['z']);
		if (array_key_exists('w', $kwargs)) {
			$this->_w = floatval(intval($kwargs['w']));
		}
		if (array_key_exists('color', $kwargs)) {
			$this->_Color = $kwargs['color'];
		} else {
			$this->_Color = new Color( array('rgb' => 0xFFFFFF));
		}
		if (self::$verbose === True)
			print("Vertex( x: ".sprintf("%.2f", $this->_x).", y: ".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).", ".$this->_Color." ) constructed".PHP_EOL);
	}
	public function __destruct() {
		if (self::$verbose === True)
			print("Vertex( x: ".sprintf("%.2f", $this->_x).", y: ".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).", ".$this->_Color." ) destructed".PHP_EOL);
	}
	function __toString() {
		if (self::$verbose === True)
			return ("Vertex( x: ".sprintf("%.2f", $this->_x).", y: ".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w).", ".$this->_Color." )");
		else
			return ("Vertex( x: ".sprintf("%.2f", $this->_x).", y: ".sprintf("%.2f", $this->_y).", z:".sprintf("%.2f", $this->_z).", w:".sprintf("%.2f", $this->_w)." )");
	}
	static function doc() {
		return (file_get_contents('Vertex.doc.txt'));
	}
	function getX() {
		return ($this->_x);
	}
	function getY() {
		return ($this->_y);
	}
	function getZ() {
		return ($this->_z);
	}
	function getW() {
		return ($this->_w);
	}
	function getColor() {
		return $this->_Color;
	}
}
?>
