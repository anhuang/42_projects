
<- Matrix ----------------------------------------------------------------------
With matrices, we will be able to start making transformations, such as applying a change of scale, a
translation or rotation to one or more vertices.
This Matrix class have 7 class constants: IDENTITY, SCALE, RX, RY, RZ, TRANSLATION and PROJECTION.
He take some parameters:
'preset' => The identity matrix
'scale'  => The scales of change of scale
'angle'  => rotation angle in radians with RX, RY, RZ
’vtc’    => The translation matrices
’fov’, 'ratio', 'near', 'far' => Projection matrices

Matrix mult (Matrix $ rhs)				: returns a new result matrix of the multiplication of the two matrices.
Vertex transformVertex (Vertex $ vtx)	: returns a new vertex result of the transformation of the vertex by the matrix.
---------------------------------------------------------------------- Matrix ->
