<?php
require_once "../ex02/Vector.class.php";

class Matrix
{
	const IDENTITY		= 1;
	const SCALE			= 2;
	const RX			= 3;
	const RY			= 4;
	const RZ			= 5;
	const TRANSLATION	= 6;
	const PROJECTION	= 7;
	public $matrix;
	public static $verbose = FALSE;
	private $type;
	function __construct($kwargs) {
		for ($i = 0; $i < 4; $i++) {
			for ($j = 0; $j < 4; $j++) {
			$this->matrix[$i][$j] = 0;
			}
		}
		if (array_key_exists('preset', $kwargs)) {
			if ($kwargs['preset'] == self::IDENTITY) {
				$this->type = "IDENTITY";
				$this->identity();
			} else if (array_key_exists('scale', $kwargs) && $kwargs['preset'] == self::SCALE) {
				$this->type = "SCALE preset";
				$this->scale($kwargs['scale']);
			} else if (array_key_exists('angle', $kwargs) && $kwargs['preset'] == self::RX ||
															$kwargs['preset'] == self::RY ||
															$kwargs['preset'] == self::RZ) {
				$this->identity();
				$this->angle($kwargs['angle'], $kwargs['preset']);
			} else if (array_key_exists('vtc', $kwargs) && $kwargs['preset'] == self::TRANSLATION) {
				$this->type = "TRANSLATION preset";
				$this->identity();
				$this->translation($kwargs['vtc']);
			} else if (array_key_exists('fov', $kwargs) && array_key_exists('ratio', $kwargs) &&
						array_key_exists('near', $kwargs) && array_key_exists('far', $kwargs)
						&& $kwargs['preset'] == self::PROJECTION) {
				$this->type = "PROJECTION preset";
				$this->projection($kwargs['fov'], $kwargs['ratio'], $kwargs['near'], $kwargs['far']);
			}
		}
		if (self::$verbose === TRUE)
			print("Matrix ".sprintf("%s",$this->type)." instance constructed".PHP_EOL);
	}
	public function __destruct() {
		if (self::$verbose === TRUE)
			print("Matrix instance destructed".PHP_EOL);
	}
	function __toString() {
		$ret = 			"M | vtcX | vtcY | vtcZ | vtxO".PHP_EOL;
		$ret .= 		"-----------------------------".PHP_EOL;
		$ret .= sprintf("x | %.2f | %.2f | %.2f | %.2f" . PHP_EOL, $this->matrix[0][0], $this->matrix[0][1], $this->matrix[0][2], $this->matrix[0][3]);
		$ret .= sprintf("y | %.2f | %.2f | %.2f | %.2f" . PHP_EOL, $this->matrix[1][0], $this->matrix[1][1], $this->matrix[1][2], $this->matrix[1][3]);
		$ret .= sprintf("z | %.2f | %.2f | %.2f | %.2f" . PHP_EOL, $this->matrix[2][0], $this->matrix[2][1], $this->matrix[2][2], $this->matrix[2][3]);
		$ret .= sprintf("w | %.2f | %.2f | %.2f | %.2f", $this->matrix[3][0], $this->matrix[3][1], $this->matrix[3][2], $this->matrix[3][3]);
		return $ret;
	}
	static function doc() {
		return (file_get_contents('Matrix.doc.txt'));
	}
	private function identity() {
		$this->matrix[0][0] = 1;
		$this->matrix[1][1] = 1;
		$this->matrix[2][2] = 1;
		$this->matrix[3][3] = 1;
	}
	private function translation($vtc) {
		$this->matrix[0][3] = $vtc->getX();
		$this->matrix[1][3] = $vtc->getY();
		$this->matrix[2][3] = $vtc->getZ();
		$this->matrix[3][3] = 1;
	}
	private function scale($scale) {
		$this->matrix[0][0] = floatval($scale);
		$this->matrix[1][1] = floatval($scale);
		$this->matrix[2][2] = floatval($scale);
		$this->matrix[3][3] = 1;
	}
	private function angle($angle, $preset) {
		$a = floatval($angle);
		if ($preset == self::RX) {
			$this->type = "Ox ROTATION preset";
			$this->matrix[0][0] = 1.0;
			$this->matrix[1][1] = cos($a);
			$this->matrix[1][2] = -sin($a);
			$this->matrix[2][1] = sin($a);
			$this->matrix[2][2] = cos($a);
		} else if ($preset == self::RY) {
			$this->type = "Oy ROTATION preset";
			$this->matrix[0][0] = cos($a);
			$this->matrix[0][2] = sin($a);
			$this->matrix[1][1] = 1;
			$this->matrix[2][0] = -sin($a);
			$this->matrix[2][2] = cos($a);
		} else if ($preset == self::RZ) {
			$this->type = "Oz ROTATION preset";
			$this->matrix[0][0] = cos($a);
			$this->matrix[0][1] = -sin($a);
			$this->matrix[1][0] = sin($a);
			$this->matrix[1][1] = cos($a);
			$this->matrix[2][2] = 1;
		}
	}
	private function projection($fov, $ratio, $near, $far) {
		$tangent = tan($fov / 2 * M_PI / 180) * $near;
		$r = $ratio * $tangent;
		$l = -$r;
		$back = -$tangent;
		$this->matrix[0][0]  =  (2 * $near) / ($r - $l);
		$this->matrix[0][2]  =  ($r + $l) / ($r - $l);
		$this->matrix[1][1]  =  (2 * $near) / ($tangent - $back);
		$this->matrix[1][2]  =  ($tangent + $back) / ($tangent - $back);
		$this->matrix[2][2]  = -($far + $near) / ($far - $near);
		$this->matrix[2][3]  = -(2 * $far * $near) / ($far - $near);
		$this->matrix[3][2]  = -1;
	}
	function getValue($x, $y) {
		return ($this->matrix[$x][$y]);
	}
	function mult($rhs) {
		self::$verbose = FALSE;
		$ret = new Matrix(array());
		self::$verbose = TRUE;
		for($i = 0; $i < 4; ++$i) {
			for ($j = 0; $j < 4; ++$j) {
				for ($k = 0; $k < 4; ++$k) {
					$ret->matrix[$i][$j] += $this->matrix[$i][$k] * $rhs->getValue($k, $j);
				}
			}
		}
		return $ret;
	}
	function transformVertex($vtx) {
		$x = $this->matrix[0][0] * $vtx->getX() +
			$this->matrix[0][1] * $vtx->getY() +
			$this->matrix[0][2] * $vtx->getZ() +
			$this->matrix[0][3] * $vtx->getW();
		$y = $this->matrix[1][0] * $vtx->getX() +
			$this->matrix[1][1] * $vtx->getY() +
			$this->matrix[1][2] * $vtx->getZ() +
			$this->matrix[1][3] * $vtx->getW();
		$z = $this->matrix[2][0] * $vtx->getX() +
			$this->matrix[2][1] * $vtx->getY() +
			$this->matrix[2][2] * $vtx->getZ() +
			$this->matrix[2][3] * $vtx->getW();
		$w = $this->matrix[3][0] * $vtx->getX() +
			$this->matrix[3][1] * $vtx->getY() +
			$this->matrix[3][2] * $vtx->getZ() +
			$this->matrix[3][3] * $vtx->getW();
		return new Vertex(array('x' => $x, 'y' => $y, 'z' => $z, 'w' => $w));
	}
}

?>
