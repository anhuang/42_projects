#!/usr/bin/php
<?PHP
if ($argc != 4)
{
	print("Incorrect Parameters\n");
	exit(0);
}
$str = trim($argv[2]);
if (!is_numeric(trim($argv[1])) || !is_numeric(trim($argv[3])) || strlen($str) != 1 ||
	(strchr($argv[2], '/') && $argv[3] == 0) || (strchr($argv[2], '%') && $argv[3] == 0))
	exit(0);
if (strchr($argv[2], '+'))
	printf("%d\n",$argv[1] + $argv[3]);
else if (strchr($argv[2], '-'))
	printf("%d\n",$argv[1] - $argv[3]);
else if (strchr($argv[2], '*'))
	printf("%d\n",$argv[1] * $argv[3]);
else if (strchr($argv[2], '/'))
	printf("%d\n",$argv[1] / $argv[3]);
else if (strchr($argv[2], '%'))
	printf("%d\n",$argv[1] % $argv[3]);
?>
