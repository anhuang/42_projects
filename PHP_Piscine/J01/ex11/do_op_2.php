#!/usr/bin/php
<?php
if ($argc != 2) {
	print("Incorrect Parameters\n");
	exit(0);
}
$str = sscanf($argv[1], "%d %c %d %s");
if ($str[0] && $str[2] && !$str[3] || ($str[2] == 0 && ($str[1] == '+' || $str[1] == '-')) &&
	$str[1] != '+' && $str[1] != '-' && $str[1] != '*' && $str[1] != '/' && $str[1] != '%') {
	if($str[1] == '*')
		printf("%d\n", $str[0] * $str[2]);
	else if($str[1] == '-')
		printf("%d\n", $str[0] - $str[2]);
	else if($str[1] == '/')
		printf("%d\n", $str[0] / $str[2]);
	else if($str[1] == '%')
		printf("%d\n", $str[0] % $str[2]);
	else if($str[1] == '+')
		printf("%d\n", $str[0] + $str[2]);
	else
		print("Syntax Error\n");
}
else {
	print("Syntax Error\n");
}
?>
