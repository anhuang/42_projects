#!/usr/bin/php
<?PHP
function type_cmp($str1, $str2) {
	$str1 = strtolower($str1);
	$str2 = strtolower($str2);
	if (ctype_alpha($str1))
		$type_str1 = 1;
	else if (ctype_digit($str1))
		$type_str1 = 2;
	else
		$type_str1 = 3;
	if (ctype_alpha($str2))
		$type_str2 = 1;
	else if (ctype_digit($str2))
		$type_str2 = 2;
	else
		$type_str2 = 3;
	if ($type_str1 != $type_str2)
		return ($type_str1 - $type_str2);
	return (strcmp($str1, $str2));
}
function cmp($str1, $str2) {
	$i = 0;
	while ($i < strlen($str1) && $i < strlen($str2)) {
		if (type_cmp($str1[$i], $str2[$i]) > 0)
			return (1);
		if (type_cmp($str1[$i], $str2[$i]) < 0)
			return (-1);
		$i++;
	}
	if ($i < strlen($str1))
		return (1);
	if ($i < strlen($str2))
		return (-1);
	return (0);
}
$i = 0;
if ($argv[1]) {
	foreach ($argv as $arg) {
		if ($i++) {
			$tab = explode(" ", $arg);
			foreach ($tab as $str)
				if (strlen($str))
					$tab2[] .= $str;
		}
	}
	usort($tab2, "cmp");
	foreach ($tab2 as $str)
		print($str."\n");
}
?>
