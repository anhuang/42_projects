#!/usr/bin/php
<?PHP
while(1)
{
	$i = 0;
	print("Entrez un nombre: ");
	$nbr = trim(fgets(STDIN));
	if (feof(STDIN) == TRUE)
		exit();
	while ($nbr[$i + 1])
		$i++;
	$tmp = array();
	if (is_numeric($nbr) == FALSE || !preg_match('#^[\d]{0,}$#', $nbr, $tmp))
		print("'$nbr' n'est pas un chiffre\n");
	else if ($nbr[$i] % 2 == 1)
		print("Le chiffre $nbr est Impair\n");
	else
		print("Le chiffre $nbr est Pair\n");
}
?>
