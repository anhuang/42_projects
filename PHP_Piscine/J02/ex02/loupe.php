#!/usr/bin/php
<?php
function recursive_child($parent){
	$node = $parent->childNodes;
	foreach ($node as $child){
		if ($child->nodeType === 3){
			$child->nodeValue = strtoupper($child->nodeValue);
			continue;
		}
		if ($child->getAttribute('title') != "")
			$child->setAttribute('title', strtoupper($child->getAttribute('title')));
		if ($child->childNodes->length > 0)
			recursive_child($child);
	}
}

if ($argc == 2){
	$html = file_get_contents($argv[1]);

	$dom = new DOMDocument;
	$dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
	$links = $dom->getElementsByTagName('a');
	foreach ($links as $link) {
		if ($link->getAttribute('title') != "")
			$link->setAttribute('title', strtoupper($link->getAttribute('title')));
		recursive_child($link);
	}
	$res = $dom->saveHTML();
$res = preg_replace_callback("/<br>/", function($matches){
	return "<br />";
},$res);
$res = preg_replace_callback("#((href)|(src))=\"[^,\"]+\"#", function($matches){
	return preg_replace("#\"#", '', $matches[0]);
},$res);
	echo $res;
}
?>
