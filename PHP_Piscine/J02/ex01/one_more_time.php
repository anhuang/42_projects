#!/usr/bin/php
<?php
date_default_timezone_set('Europe/Paris');
function get_month($str)
{
	if ($str == "janvier" || $str == "Janvier")
		return (1);
	else if ($str == "fevrier" || $str == "Fevrier" || $str == "Février" || $str == "février")
		return (2);
	else if ($str == "mars" || $str == "Mars")
		return (3);
	else if ($str == "avril" || $str == "Avril")
		return (4);
	else if ($str == "mai" || $str == "Mai")
		return (5);
	else if ($str == "Juin" || $str == "juin")
		return (6);
	else if ($str == "juillet" || $str == "Juillet")
		return (7);
	else if ($str == "Aout" || $str == "aout" || $str == "Août" || $str == "août")
		return (8);
	else if ($str == "septembre" || $str == "Septembre")
		return (9);
	else if ($str == "octobre" || $str == "Octobre")
		return (10);
	else if ($str == "novembre" || $str == "Novembre")
		return (11);
	else if ($str == "decembre" || $str == "Décembre" || $str == "Decembre" || $str == "décembre")
		return (12);
}
if ((preg_match("/(^[L|l]undi|^[M|m]ardi|^[M|m]ercredi|^[J|j]eudi|^[V|v]endredi|^[S|s]amedi|^[D|d]imanche) {1}([0-9]{1,2}) {1}([J|j]anvier|[F|f][e|é]vrier|[M|m]ars|[A|a]vril|[M|m]ai|[J|j]uin|[J|j]uillet|[A|a]o[u|û]t|[S|s]eptembre|[O|o]ctobre|[N|n]ovembre|[D|d][e|é]cembre) {1}([0-9]{4}) {1}([0-9]{2}):[0-9]{2}:[0-9]{2}$/", $argv[1]) != 0))
{
	preg_match('/\d{1,2}:/', $argv[1], $h);
	$h[0] = substr($h[0], 0, 2);
	preg_match('/:\d{1,2}:/', $argv[1], $min);
	$min[0] = substr($min[0], 1, 2);
	preg_match_all('/:\d{1,2}/', $argv[1], $sec);
	$sec[0][1] = substr($sec[0][1], 1, 2);
	preg_match('/([J|j]anvier|[F|f][e|é]vrier|[M|m]ars|[A|a]vril|[M|m]ai|[J|j]uin|[J|j]uillet|[A|a]o[u|û]t|[S|s]eptembre|[N|n]ovembre|[D|d][e|é]cembre)/', $argv[1], $monthe);
	$month = get_month($monthe[0]);
	preg_match('/\d{1,2}/', $argv[1], $day);
	preg_match('/\d{4}/', $argv[1], $year);
	print(mktime($h[0], $min[0], $sec[0][1], $month, $day[0], $year[0])."\n");
}
else
	print("Wrong Format\n");
?>
