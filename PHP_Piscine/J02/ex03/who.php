#!/usr/bin/php
<?php
date_default_timezone_set('Europe/paris');
$usr = get_current_user();
$file = file_get_contents("/var/run/utmpx");
$ar = 'Z256usr/Z4id/Z32line/ipid/itype/i2time';
$tab = array();
while ($file != NULL){
	$info = unpack($ar, $file);
	if (strcmp(trim($info[usr]), $usr) == 0 && $info[type] == 7){
		$line = trim($info[usr]);
		$line .= "  ".trim($info[line]);
		$line .= "  ".date("M j H:i",$info[time1]);
		$tab = array_merge($tab, array($line));
	}
	$file = substr($file, 628);
}
sort($tab);
foreach ($tab as $pre)
	echo $pre."\n";
?>
