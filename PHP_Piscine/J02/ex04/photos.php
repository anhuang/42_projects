#!/usr/bin/php
<?php
function grab_image($url,$saveto){
	$ch = curl_init ($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	$raw=curl_exec($ch);
	curl_close ($ch);
	if(file_exists($saveto)){
		unlink($saveto);
	}
	$fp = fopen($saveto,'x');
	fwrite($fp, $raw);
	fclose($fp);
}
if ($argc == 2){
	$html = file_get_contents($argv[1]);
	$dom = new DOMDocument;
	$dom->loadHTML($argv[1]);
	$links = $dom->getElementsByTagName('img');
	foreach ($links as $link) {
		if ($link->getAttribute('src') != "")
			print(($link->getAttribute('src'))."\n");
		// recursive_child($link);
	}
	$res = $dom->saveHTML();
	// $c = curl_init();
	// curl_exec($c);
}
?>
