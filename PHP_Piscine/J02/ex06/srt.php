#!/usr/bin/php
<?php
function swap($array, $i1, $i2){
	$tmp = $array[$i1];
	$array[$i1] = $array[$i2];
	$array[$i2] = $tmp;
	$tmp = $array[$i1 + 1];
	$array[$i1 + 1] = $array[$i2 + 1];
	$array[$i2 + 1] = $tmp;
	return ($array);
}
if ($argc == 2){
	$read = file($argv[1]);
	$i = 0;
	preg_replace("/\n$/", "",$read);
	while ($read[$i + 3]){
		if (preg_match("/\d{2}:\d{2}:\d{2},\d{3}/", $read[$i]) && preg_match("/\d{2}:\d{2}:\d{2},\d{3}/", $read[$i + 3])){
			if (strcmp(implode(preg_grep("/\d{2}:\d{2}:\d{2},\d{3}/",array($read[$i]))), implode(preg_grep("/\d{2}:\d{2}:\d{2},\d{3}/",array($read[$i + 3])))) > 0){
				$read = swap($read, $i, $i + 3);
				$i = 0;
			}
		}
		$i++;
	}
	foreach($read as $read_s)
		print($read_s);
}
?>
