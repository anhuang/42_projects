<?php

//Category
if ($_POST["submit"] == "Add" && ($_POST["product"] == "" || $_POST["product"] == "(optionnel)"))
{
	if (file_exists("../DB/category/".$_POST["category"]))
	{
		//error
		header("Location: admin.php");
		exit;
	}
	mkdir ("../DB/category/".$_POST["category"], 0755);
	echo "OK1";
}
else if ($_POST["submit"] == "Remove" && ($_POST["product"] == "" || $_POST["product"] == "(optionnel)"))
{
	if (!file_exists("../DB/category/".$_POST["category"]))
	{
		//error
		header("Location: admin.php");
		exit;
	}
	$dir = scandir("../DB/category/".$_POST["category"]);
	foreach ($dir as $elem)
	{
		if ($elem != "." && $elem != "..")
			unlink("../DB/category/".$_POST["category"]."/".$elem);
	}
	rmdir ("../DB/category/".$_POST["category"]);
	echo "OK2";
}

//Produits
else if ($_POST["submit"] == "Add")
{
	if (!file_exists("../DB/category/".$_POST["category"]))
	{
		mkdir ("../DB/category/".$_POST["category"], 0755);
	}
	if (file_exists("../DB/category/".$_POST["category"]."/".$_POST["product"]))
	{
		//error
		header("Location: admin.php");
		exit;
	}
	if (!$_POST["img"] || !$_POST["price"])
	{
		//error
		header("Location: admin.php");
		exit;
	}
	$product["img"] = "../img/".$_POST["img"];
	$product["price"] = $_POST["price"];
	file_put_contents("../DB/category/".$_POST["category"]."/".$_POST["product"], serialize($product));
	echo "OK3";
}
else if ($_POST["submit"] == "Remove")
{
	if (!file_exists("../DB/category/".$_POST["category"]."/".$_POST["product"]))
	{
		//error
		header("Location: admin.php");
		exit;
	}
	unlink("../DB/category/".$_POST["category"]."/".$_POST["product"]);
	echo "OK4";
}
else if ($_POST["submit"] == "Modif")
{
	if (!file_exists("../DB/category/".$_POST["category"]."/".$_POST["product"]))
	{
		//error
		header("Location: admin.php");
		exit;
	}
	$arr = unserialize(file_get_contents("../DB/category/".$_POST["category"]."/".$_POST["product"]));
	if ($_POST["price"])
		$arr["price"] = $_POST["price"];
	if ($_POST["img"])
		$arr["img"] = "../img/".$_POST["img"];
	file_put_contents("../DB/category/".$_POST["category"]."/".$_POST["product"], serialize($arr));
	echo "OK5";
}
header('Refresh: 3; URL=admin.php');

 ?>
