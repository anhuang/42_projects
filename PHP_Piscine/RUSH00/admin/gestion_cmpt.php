<?php

if ($_POST["submit"] == "Add")
{
	if (!$_POST["login"] || !$_POST["passwd"] || $_POST["passwd"] == "(optionnel)")
	{
		//message erreur
		header("Location: admin.php");
		exit;
	}

	$cmpt["login"] = $_POST["login"];
	$cmpt["passwd"] = hash("whirlpool", $_POST["passwd"]);
	$cmpt["admin"] = 0;
	$cmpt["panier"]= array();
	$cmpt['panier']['name']=array();
	$cmpt['panier']['qte']=array();
	$cmpt['panier']['price']=array();

	if (file_exists("../DB/Users/".$cmpt['login']))
	{
		//message erreur
		header("Location: admin.php");
		exit;
	}

	file_put_contents("../DB/Users/".$cmpt['login'], serialize($cmpt));
	echo "OK";
}
else if ($_POST["submit"] == "Remove")
{
	if (!$_POST["login"] || $_POST["login"] == "root")
	{
		//error
		header("Location: admin.php");
		exit;
	}
	$cmpt["login"] = $_POST["login"];
	if (!file_exists("../DB/Users/".$cmpt['login']))
	{
		//message erreur
		header("Location: admin.php");
		exit;
	}
	unlink("../DB/Users/".$cmpt['login']);
	echo "OK";
}
else if ($_POST["submit"] == "Change mdp")
{
	$cmpt["login"] = $_POST["login"];
	$cmpt["passwd"] = hash("whirlpool", $_POST["passwd"]);

	if (!file_exists("../DB/Users/".$cmpt["login"]) || $_POST["login"] == "root")
	{
		//error
		header("Location: admin.php");
		exit;
	}

	$arr = unserialize(file_get_contents("../DB/Users/".$cmpt["login"]));

	$arr["passwd"] = $cmpt["passwd"];
	file_put_contents("../DB/Users/".$cmpt["login"], serialize($arr));
	echo "OK";
}
else if ($_POST["submit"] == "Admin or not Admin")
{
	$cmpt["login"] = $_POST["login"];

	if (!file_exists("../DB/Users/".$cmpt["login"]) || $_POST["login"] == "root")
	{
		//error
		header("Location: admin.php");
		exit;
	}

	$arr = unserialize(file_get_contents("../DB/Users/".$cmpt["login"]));

	$arr["admin"] = ($arr["admin"] + 1) % 2;
	file_put_contents("../DB/Users/".$cmpt["login"], serialize($arr));
	echo "OK";
}

header('Refresh: 3; URL=admin.php');
 ?>
