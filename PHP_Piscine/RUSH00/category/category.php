<?php
session_start();
if (!isset($_SESSION['panier']))
{
	$_SESSION['panier']=array();
	$_SESSION['panier']['name']=array();
	$_SESSION['panier']['qte']=array();
	$_SESSION['panier']['price']=array();
}

function ajout_panier($category, $product)
{
	if (!$product)
	return;
	$pos = array_search($product,  $_SESSION['panier']['name']);
	if ($pos !== false)
	{
		$_SESSION['panier']['qte'][$pos] += 1 ;
		if (file_exists("../DB/Users/".$_SESSION['login']))
		{
		$arr = unserialize(file_get_contents("../DB/Users/".$_SESSION['login']));
		$arr['panier']['qte'][$pos] += 1;
		file_put_contents("../DB/Users/".$_SESSION["login"], serialize($arr));
	}
	}
	else
	{
		$name = preg_replace("/\.\w+/", "", $product);
		$path = "../DB/Category/".$category."/".$name;
		$tmp = unserialize(file_get_contents($path));
		array_push( $_SESSION['panier']['name'],$product);
		array_push( $_SESSION['panier']['qte'],1);
		array_push( $_SESSION['panier']['price'],$tmp['price']);
		if ($_SESSION["login"])
		{
			if (file_exists("../DB/Users/".$_SESSION['login']))
			{
				$arr = unserialize(file_get_contents("../DB/Users/".$_SESSION['login']));

				array_push( $arr['panier']['name'],$product);
				array_push( $arr['panier']['qte'],1);
				array_push( $arr['panier']['price'],$tmp['price']);
				file_put_contents("../DB/Users/".$_SESSION["login"], serialize($arr));
			}
		}
	}
}

function enlever_panier($_product)
{
	$tmp=array();
	$tmp['name'] = array();
	$tmp['qte'] = array();
	$tmp['price'] = array();

	for($i = 0; $i < count($_SESSION['panier']['name']); $i++)
	{
	   if ($_SESSION['panier']['name'][$i] !== $product)
	   {
		  array_push( $tmp['name'],$_SESSION['panier']['name'][$i]);
		  array_push( $tmp['qte'],$_SESSION['panier']['qteProduit'][$i]);
		  array_push( $tmp['price'],$_SESSION['panier']['prixProduit'][$i]);
	   }

	}
	$_SESSION['panier'] =  $tmp;
	$arr = unserialize(file_get_contents("../DB/Users/".$_SESSION['login']));
	$arr['panier'] = $tmp;
	file_put_contents("../DB/Users/".$cmpt["login"], serialize($arr));
	unset($tmp);
}

function supprimer_panier()
{
	unset ($_SESSION['panier']);
	$_SESSION['panier']=array();
	$_SESSION['panier']['name']=array();
	$_SESSION['panier']['qte']=array();
	$_SESSION['panier']['price']=array();
	$arr = unserialize(file_get_contents("../DB/Users/".$_SESSION['login']));
	$arr['panier'] = array();
	file_put_contents("../DB/Users/".$cmpt["login"], serialize($arr));

}

if ($_POST["submit"] == "Ajouter au panier")
{

	ajout_panier($_POST["category"], $_POST["product"]);
}

function get_img($category, $product) {
	$name = preg_replace("/\.\w+/", "", $product);
	$path = "../DB/Category/".$category."/".$name;
	$content = unserialize(file_get_contents($path));
	return ($content["img"]);
}
?>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="category.css">
		<link rel="stylesheet" href="../accueil/index.css">
	</head>
	<header class="Header">
		<a href="../accueil/index.php" ><img class="logo" src="../img/logo.png"></a>
		<div style="display:flex;">
			<?php
				if ($_SESSION['login'])
				{
					$usr = unserialize(file_get_contents("../DB/Users/".$_SESSION["login"]));
					if ($usr["admin"] == 1)
						echo "<a href='../admin/admin.php' ><img class='Admin' src='../img/admin.jpeg' ></a>";
				}
			?>
		<a href="../compte/compte.php" ><img class="Users" src="../img/Users.png" ></a>
		<a href="../panier/panier.php"><li class="rar2"><img class="panier" src="../img/Basket.jpg"></a>
			<ul class="niveau basket">
				<?php
				session_start();
				$handle = opendir("../DB/Users/");
				$i = 0;
				$change = 0;
				$total_price = 0;
				while (($users = readdir($handle)) !== false) {
					if ($users == $_SESSION['login']) {
						?><div><?php
						while ($_SESSION['panier']['name'][$i]) {
							if ($_SESSION['panier']['qte'][$i] > 0) {
								?><li class="panier_product"><?=$_SESSION['panier']['name'][$i]?>(<?=$_SESSION['panier']['qte'][$i]?>)</li><?php
								$total_price += $_SESSION['panier']['price'][$i] * $_SESSION['panier']['qte'][$i];
								$change = 1;
							}
							$i++;
						} ?></div><?php
					}
				}
				session_start();
				if ($change == 0) {
					$i = 0;
					while($_SESSION['panier']['name'][$i]) {
						if ($_SESSION['panier']['qte'][$i] > 0) {
							?><li class="panier_product"><?=$_SESSION['panier']['name'][$i]?>(<?=$_SESSION['panier']['qte'][$i]?>)</li><?php
							$total_price += $_SESSION['panier']['price'][$i] * $_SESSION['panier']['qte'][$i];
						}
						$i++;
					}
				}?>
			<li class="price_total">TOTAL : <?=$total_price?> €</li>
			<a href="../panier/panier.php"><li class="price_total" >Voir mon panier</li></a>
			</ul>
		</li>
		</div>
	</header>
	<body>
	<div class="category_menu">
		<li class="rar" style="width:215px;" ><a href="../category/category.php"><img class="logo_menu" src="../img/Menu.svg"></a>
			<ul class="niveau">
			<?php $handle = opendir("../DB/Category/");
			$category = readdir($handle);
			$category = readdir($handle);
			while (($category = readdir($handle)) !== false) {
				?><a href="#<?=$category?>"><li class="menu_category"><?=$category?></li></a> <?php
			} ?>
			</ul>
		</li>
	</div>
	<?php
	$handle = opendir("../DB/Category/");
	$category = readdir($handle);
	$category = readdir($handle);
	while (($category = readdir($handle)) !== false) {
		$folder = opendir("../DB/Category/$category");
		$file = readdir($folder);
		$file = readdir($folder); ?>
		<h2 id="<?=$category?>" class="text_cat"><?=strtoupper($category)?></h2>
		<div class="category_box"> <?php
		while (($file = readdir($folder)) !== false) { ?>
			<figure>
				<img class="Ordi" src="<?= get_img("$category", "$file.jpg");?>" />
				<form action="" method="post">
				<input type="hidden" name="category" value=<?=$category?>>
				<input type="hidden" name="product" value=<?=$file?>>
				<input class="add_panier" type="submit" name="submit" value="Ajouter au panier"/>
				</form>
			</figure>
		<?php }
	?></div> <?php
	}
	?>
	</body>
</html>
