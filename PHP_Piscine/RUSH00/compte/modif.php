<?php
session_start();

if ($_POST["submit"] == "Logout")
{
	setcookie("login", "", time() - 3600);
	$_SESSION["login"] = "";
	unset($_SESSION["panier"]);
	$_SESSION['panier']=array();
	$_SESSION['panier']['name']=array();
	$_SESSION['panier']['qte']=array();
	$_SESSION['panier']['price']=array();
	header("Location: compte.php");
	exit;
}

if ($_POST["submit"] == "Modif")
{
	$cmpt=array();

	$cmpt["login"] = $_POST["login"];
	$cmpt["oldpw"] = hash("whirlpool", $_POST["oldpw"]);
	$cmpt["newpw"] = hash("whirlpool", $_POST["newpw"]);

	if (file_exists("../DB/Users/".$cmpt['login']))
		$arr = unserialize(file_get_contents("../DB/Users/".$cmpt['login']));
	else
	{
		header("Location: fail.php");
		exit;
	}

	if (!file_exists("../DB/Users/".$cmpt["login"]) || $_POST["login"] == "root")
	{
		//error
		header("Location: fail.php");
		exit;
	}

	$arr = unserialize(file_get_contents("../DB/Users/".$cmpt["login"]));

	if ($arr["passwd"] != $cmpt["oldpw"])
	{
		//error
		header("Location: fail.php");
		exit;
	}
	$arr["passwd"] = $cmpt["newpw"];
	file_put_contents("../DB/Users/".$cmpt["login"], serialize($arr));
}

if ($_POST['submit'] == "Suppr")
{
	if (!$_POST["login"] || !$_POST["login"] == "root" || $_POST['login'] != $_SESSION['login'])
	{
		//error
		header("Location: fail.php");
		exit;
	}
	$cmpt["login"] = $_POST["login"];
	$cmpt["passwd"] = hash("whirlpool", $_POST["passwd"]);
	if (!file_exists("../DB/Users/".$cmpt['login']))
	{
		//message erreur
		header("Location: fail.php");
		exit;
	}
	$usr = unserialize(file_get_contents("../DB/Users/".$cmpt["login"]));
	if ($cmpt['passwd'] != $usr['passwd'])
	{
		header("Location: fail.php");
		exit;
	}
	unlink("../DB/Users/".$cmpt['login']);
	$_SESSION['login'] = "";
}


header('Refresh: 3; URL=../accueil/index.php');

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Compte créé</title>
		<link rel="stylesheet" href="../category/category.css">
	</head>
	<header class="Header">
		<a href="../accueil/index.php" ><img class="logo" src="../img/logo.png"></a>
		<a href="../panier/panier.php" ><img class="panier" src="../img/Basket.jpg"></a>
		<a href="../compte/compte.php" ><img class="Users" src="../img/Users.png" ></a>
		<?php
			if ($_SESSION['login'])
			{
				$usr = unserialize(file_get_contents("../DB/Users/".$_SESSION["login"]));
				if ($usr["admin"] == 1)
					echo "<a href='../admin/admin.php' ><img class='Users' src='../img/admin.jpeg' ></a>";
			}
		?>
	</header>
	<body>
		<br>
		Action réalisée avec succès.
		<br>
		Attendez 3 secondes ou cliquez
		<a href="../accueil/index.php">ici pour être rediriger vers le site</a>
	</body>
</html>
