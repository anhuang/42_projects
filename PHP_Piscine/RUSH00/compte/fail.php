<?php
header('Refresh: 3; URL=compte.php');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Fail</title>
		<link rel="stylesheet" href="../accueil/index.css">
	</head>
	<header class="Header">
		<a href="../accueil/index.php" ><img class="logo" src="../img/logo.png"></a>
		<a href="../panier/panier.php" ><img class="panier" src="../img/Basket.jpg"></a>
		<a href="../compte/compte.php" ><img class="Users" src="../img/Users.png" ></a>
		<?php
			if ($_SESSION['login'])
			{
				$usr = unserialize(file_get_contents("../DB/Users/".$_SESSION["login"]));
				if ($usr["admin"] == 1)
					echo "<a href='../admin/admin.php' ><img class='Users' src='../img/admin.jpeg' ></a>";
			}
		?>
	</header>
	<body background="../img/Baptista.png">
		<br>
		Une erreur est survenue, attendez 3 secondes ou
		<a href="compte.php">cliquez ici pour reessayer</a>.
	</body>

</html>
