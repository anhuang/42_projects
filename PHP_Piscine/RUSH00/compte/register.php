<?php
session_start();

if ($_POST["submit"] != "Créer" || !$_POST["login"] || !$_POST["passwd"])
{
	header("Location: fail.php");
	exit;
}

$cmpt=array();
$cmpt["login"] = $_POST["login"];
$cmpt["passwd"] = hash("whirlpool", $_POST["passwd"]);
$cmpt["admin"] = 0;
$cmpt['panier']=array();
$cmpt['panier']['name']=array();
$cmpt['panier']['qte']=array();
$cmpt['panier']['price']=array();

if (file_exists("../DB/Users/".$cmpt['login']))
{
	header("Location: fail.php");
	exit;
}

file_put_contents("../DB/Users/".$cmpt['login'], serialize($cmpt));
setcookie("login", $cmpt["login"]);
$_SESSION["login"] = $cmpt["login"];
unset($_SESSION["panier"]);
$_SESSION["panier"] = $cmpt["panier"];
header('Refresh: 3; URL=../accueil/index.php');
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Compte créé</title>
		<link rel="stylesheet" href="../category/category.css">
	</head>
	<header class="Header">
		<a href="../accueil/index.php" ><img class="logo" src="../img/logo.png"></a>
		<a href="../panier/panier.php" ><img class="panier" src="../img/Basket.jpg"></a>
		<a href="../compte/compte.php" ><img class="Users" src="../img/Users.png" ></a>
		<?php
			if ($_SESSION['login'])
			{
				$usr = unserialize(file_get_contents("../DB/Users/".$_SESSION["login"]));
				if ($usr["admin"] == 1)
					echo "<a href='../admin/admin.php' ><img class='Users' src='../img/admin.jpeg' ></a>";
			}
		?>
	</header>
	<body>
		<br>
		Action réalisée avec succès.
		<br>
		Attendez 3 secondes ou cliquez
		<a href="../accueil/index.php">ici pour être rediriger vers le site</a>
	</body>
</html>
