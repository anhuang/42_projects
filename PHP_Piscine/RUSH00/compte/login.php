<?php
session_start();

if ($_POST["submit"] != "Login" || !$_POST["login"] || !$_POST["passwd"])
{
	header("Location: compte.php");
	exit;
}

$cmpt=array();
$cmpt["login"] = $_POST["login"];
$cmpt["passwd"] = hash("whirlpool", $_POST["passwd"]);

if (!file_exists("../DB/Users/".$cmpt["login"]))
{
	header("Location: compte.php");
	exit;
}

$user = unserialize(file_get_contents("../DB/Users/".$cmpt["login"]));

if ($user["passwd"] != $cmpt["passwd"])
{
	header("Location: compte.php");
	exit;
}

$_SESSION["login"] = $_POST["login"];

	unset($_SESSION["panier"]);
	$_SESSION['panier']=array();
	$_SESSION['panier']['name']=array();
	$_SESSION['panier']['qte']=array();
	$_SESSION['panier']['price']=array();
	$_SESSION['panier'] = $user['panier'];

header("Location: ../accueil/index.php");

 ?>
