<?php
session_start();
if (!$_SESSION["login"])
{
	require("nolog.html");
	header('Refresh: 3; URL=../compte/compte.php');
	exit;
}
else if (!$_SESSION['panier']['name']['0'])
{
	//echo "Erreur panier vide";
	require("nocart.html");
	header('Refresh: 2; URL=panier.php');
	exit;
}

if (!is_dir("../DB/Commandes"))
	mkdir("../DB/Commandes");
	if (!is_dir("../DB/Commandes/".$_SESSION['login']))
		mkdir("../DB/Commandes/".$_SESSION['login']);

$i = 1;
while (file_exists("../DB/Commandes/".$_SESSION['login']."/".$i))
	$i++;

file_put_contents("../DB/Commandes/".$_SESSION['login']."/".$i, serialize($_SESSION['panier']));

unset ($_SESSION['panier']);
$_SESSION['panier']=array();
$_SESSION['panier']['name']=array();
$_SESSION['panier']['qte']=array();
$_SESSION['panier']['price']=array();
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Compte</title>
	<link rel="stylesheet" href="panier.css">
	<link rel="stylesheet" href="../accueil/index.css">
</head>
<header class="Header">
	<a href="../accueil/index.php" ><img class="logo" src="../img/logo.png"></a>
	<a href="../panier/panier.php" ><img class="panier" src="../img/Basket.jpg"></a>
	<a href="../compte/compte.php" ><img class="Users" src="../img/Users.png" ></a>
	<?php
		if ($_SESSION['login'])
		{
			$usr = unserialize(file_get_contents("../DB/Users/".$_SESSION["login"]));
			if ($usr["admin"] == 1)
				echo "<a href='../admin/admin.php' ><img class='Admin' src='../img/admin.jpeg' ></a>";
		}
	?>
</header>
<h1>Votre commande est bien passée. Merci pour les <?= $_SESSION["total"] ?> €
</h1>
</html>
