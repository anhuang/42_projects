function init(){
	if (!readCookie("to_do"))
		return ;
	readCookie("to_do").split("^").reverse().forEach((o)=>{
		$("#ft_list").prepend("<div>" + o + "</div>");
		$("#ft_list:first-child").click(function(){
			var answer = confirm("Voulez vous supprimer ce to_do ?");
			if (answer){
				rmInCookie("to_do", $(this).index());
				$(this).remove();
			}
		});
	});
};

function add_todo() {
	var text = prompt("To do : ");

	if (text == "" || !text || text.includes("^"))
		return ;
	$("#ft_list").prepend("<div>" + text + "</div>");
	addInCookie("to_do", text);
	$("#ft_list:first-child").click(function(){
		var answer = confirm("Voulez vous supprimer ce to_do ?");
		if (answer){
			rmInCookie("to_do", $(this).index());
			$(this).remove();
		}
	});
}

function addInCookie(name, value)
{
	var cookie = readCookie(name);
	var tab = cookie ? value + '^' + cookie : value;
	createCookie(name, tab, 2);
}

function rmInCookie(name, index){
	cookie = readCookie(name);
	tab = cookie ? cookie.split('^') : [""];
	tab.splice(index, 1);
	tab = tab.reduce((t, o)=>{
		return (t == "" ? o : t + "^" + o);
	}, "");
	createCookie(name, tab, 2);
}

function createCookie(name, value, days) {
	var expires;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	else {
		expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');

	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1,c.length);
		}
		if (c.indexOf(nameEQ) === 0) {
			return c.substring(nameEQ.length,c.length);
		}
	}
	return null;
}
