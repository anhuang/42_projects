function init(){
	if (!readCookie("to_do"))
		return ;
	var new_list = document.getElementById("ft_list");
	readCookie("to_do").split("^").reverse().forEach((o)=>{
		var to_do = document.createElement("div");
		var textnode = document.createTextNode(o);
		to_do.appendChild(textnode);
		to_do.addEventListener("click", function cliked() {
			var answer = confirm("Voulez vous supprimer ce to_do ?")
			if (answer){
				rmInCookie("to_do", Array.from(this.parentNode.children).reduce((t, o, i)=>{
					return (t == o ? i : t);
				}, this));
				this.parentNode.removeChild(this);
			}
		});
		new_list.insertBefore(to_do, new_list.childNodes[0]);
	});
};

function add_todo() {
	var to_do = document.createElement("div");
	var text = prompt("To do : ");
	var textnode = document.createTextNode(text);
	var new_list = document.getElementById("ft_list");
	var cookie;

	if (text == "" || !text)
		return ;
	to_do.appendChild(textnode);
	addInCookie("to_do", text);

	to_do.addEventListener("click", function cliked() {
		var answer = confirm("Voulez vous supprimer ce to_do ?")
		if (answer){
			rmInCookie("to_do", Array.from(this.parentNode.children).reduce((t, o, i)=>{
				return (t == o ? i : t);
			}, this));
			this.parentNode.removeChild(this);
		}
	});
	new_list.insertBefore(to_do, new_list.childNodes[0]);
}

function addInCookie(name, value)
{
	var cookie = readCookie(name);
	var tab = cookie ? value + '^' + cookie : value;
	createCookie(name, tab, 2);
}

function rmInCookie(name, index){
	cookie = readCookie(name);
	tab = cookie ? cookie.split('^') : [""];
	tab.splice(index, 1);
	tab = tab.reduce((t, o)=>{
		return (t == "" ? o : t + "^" + o);
	}, "");
	createCookie(name, tab, 2);
}

function createCookie(name, value, days) {
	var expires;
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	else {
		expires = "";
	}
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');

	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1,c.length);
		}
		if (c.indexOf(nameEQ) === 0) {
			return c.substring(nameEQ.length,c.length);
		}
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}
