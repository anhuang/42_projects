<?php
if ($_POST['login'] != NULL && $_POST['oldpw'] != NULL && $_POST['newpw'] != NULL && $_POST['submit'] == "OK") {
	$error = 1;
	$content = unserialize(file_get_contents("../private/passwd"));
	$pwd = hash('whirlpool', $_POST['newpw']);
	$oldpwd = hash('whirlpool', $_POST['oldpw']);
	$modify = 0;
	foreach ($content as $key => $value) {
		if ($content['login'] === $_POST['login'] && $oldpwd === $content['passwd']) {
			$content['passwd'] = $pwd;
			$modify = 1;
			$error = 0;
		}
	}
	if ($modify == 1) {
		file_put_contents("../private/passwd", serialize($content));
		echo "OK\n";
	}
	if ($error == 1)
		echo "ERROR\n";
} else {
	echo "ERROR\n";
}
