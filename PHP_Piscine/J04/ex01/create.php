<?php
if (!$_POST || !$_POST['login'] || !$_POST['passwd'] || $_POST['submit'] != "OK") {
	echo "ERROR\n";
} else {
	if (!file_exists("../private"))
		mkdir("../private", 0700);
	$error = 0;
	$new_user['login'] = $_POST['login'];
	$new_user['passwd'] = hash('whirlpool', $_POST['passwd']);
	$old_tab = [];
	$old_tab [] = $new_user;
	if (!file_exists("../private/passwd")) {
		file_put_contents("../private/passwd", serialize($new_user));
	} else {
		$old_tab = unserialize(file_get_contents("../private/passwd"));
		foreach ($old_tab as $key => $value) {
			if ($old_tab['login'] == $_POST['login'] || $value['login'] == $_POST['login']) {
				$error = 1;
			}
		}
		if ($error != 1) {
			$old_tab[] = $new_user;
			file_put_contents("../private/passwd", serialize($old_tab));
		}
	}
	if ($error == 0)
		echo "OK\n";
	else {
		echo "ERROR\n";
	}
}
?>
