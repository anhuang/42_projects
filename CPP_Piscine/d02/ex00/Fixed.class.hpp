#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

#include <iostream>

class Fixed
{
public:
	Fixed(void);
	Fixed(Fixed const &src);
	~Fixed(void);
	Fixed &	operator=(Fixed const &rhs);

	int getRawBits() const;
	void setRawBits(int const);

	private:
		int static const _fracBits;
		int _fixedPoint;

};

std::ostream &	operator<<(std::ostream &o, Fixed const &rhs);

#endif
