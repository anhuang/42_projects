#include "Fixed.class.hpp"

const int Fixed::_fracBits = 8;

Fixed::Fixed(int const nbr)
{
	this->_fixedPoint = nbr << Fixed::_fracBits;
	std::cout << "Int constructor called" << std::endl;
}

Fixed::Fixed(float const nbr)
{
	this->_fixedPoint = roundf(nbr * (1 << Fixed::_fracBits));
	std::cout << "Float constructor called" << std::endl;
}

Fixed::Fixed(void) : _fixedPoint(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(Fixed const &src)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}

Fixed::~Fixed(void)
{
	std::cout << "Destructor called" << std::endl;
}

Fixed &	Fixed::operator=(Fixed const &rhs)
{
	std::cout << "Assignation operator called" << std::endl;

	if (this != &rhs)
		this->_fixedPoint = rhs.getRawBits(); // equivalent to ue setRawBits
	return *this;
}

int Fixed::getRawBits(void) const {
	return this->_fixedPoint;
}

void Fixed::setRawBits(int const raw) {
	this->_fixedPoint = raw;
}

float Fixed::toFloat() const
{
	return ((float)(this->_fixedPoint) / (1 << Fixed::_fracBits));
}

int Fixed::toInt() const
{
	return ((int)(this->_fixedPoint >> Fixed::_fracBits));
}

std::ostream &operator<<( std::ostream &flux, Fixed const& rhs)
{
	flux << rhs.toFloat();
	return flux;
}
