#ifndef FIXED_CLASS_HPP
# define FIXED_CLASS_HPP

#include <iostream>
#include <cmath>

class Fixed
{
public:
	Fixed(void);
	Fixed(int const);
	Fixed(float const);
	Fixed(Fixed const &src);
	~Fixed(void);
	bool	operator==(Fixed const &) const;
	bool	operator!=(Fixed const &) const;
	bool	operator>(Fixed const &) const;
	bool	operator<(Fixed const &) const;
	bool	operator>=(Fixed const &) const;
	bool	operator<=(Fixed const &) const;

	Fixed & operator=(Fixed const &);
	Fixed operator+(Fixed const &);
	Fixed operator-(Fixed const &);
	Fixed operator*(Fixed const &);
	Fixed operator/(Fixed const &);

	Fixed& operator+=(Fixed const &);
	Fixed& operator-=(Fixed const &);
	Fixed& operator*=(Fixed const &);
	Fixed& operator/=(Fixed const &);

	Fixed& operator++();
	Fixed operator++(int);
	Fixed& operator--();
	Fixed operator--(int);

	const Fixed static & min(Fixed const &, Fixed const &);
	const Fixed static & max(Fixed const &, Fixed const &);
	Fixed static & min(Fixed &, Fixed &);
	Fixed static & max(Fixed &, Fixed &);

	int getRawBits() const;
	void setRawBits(int const);
	void setRawBits(float const);
	float toFloat() const;
	int toInt() const;
	private:
		int static const _fracBits;
		int _fixedPoint;
};

std::ostream &	operator<<(std::ostream &o, Fixed const &rhs);

#endif
