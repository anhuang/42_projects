#include "Fixed.class.hpp"

const int Fixed::_fracBits = 8;

Fixed::Fixed(void) : _fixedPoint(0) { }

Fixed::Fixed(int const nbr) {
	this->_fixedPoint = nbr << Fixed::_fracBits;
}

Fixed::Fixed(float const nbr) {
	this->_fixedPoint = roundf(nbr * (1 << Fixed::_fracBits));
}

Fixed::Fixed(Fixed const &src)
{
	*this = src;
}

Fixed::~Fixed(void) { }

int Fixed::getRawBits(void) const { return this->toInt(); }
void Fixed::setRawBits(int const raw) { this->_fixedPoint = raw; }
void Fixed::setRawBits(float const nbr) { this->_fixedPoint = roundf(nbr * (1 << Fixed::_fracBits)); }

float Fixed::toFloat() const {
	return ((float)(this->_fixedPoint) / (1 << Fixed::_fracBits));
}

int Fixed::toInt() const{
	return ((int)(this->_fixedPoint >> Fixed::_fracBits));
}

std::ostream &operator<<( std::ostream &flux, Fixed const& rhs) {
	flux << rhs.toFloat();
	return flux;
}

// NON-MEMBER FUNCTIONS OVERLOADS
const Fixed& Fixed::min(Fixed const &lhs, Fixed const &rhs) {
	return (lhs > rhs ? rhs : lhs);
}

const Fixed& Fixed::max(Fixed const &lhs, Fixed const &rhs) {
	return (lhs > rhs ? lhs : rhs);
}

Fixed& Fixed::min(Fixed &lhs, Fixed &rhs) {
	return (lhs > rhs ? rhs : lhs);
}

Fixed& Fixed::max(Fixed &lhs, Fixed &rhs) {
	return (lhs > rhs ? lhs : rhs);
}
//COMPARISON_OPERATORS

bool	Fixed::operator==(Fixed const &rhs) const {
	return (this->getRawBits() == rhs.getRawBits());
}

bool	Fixed::operator!=(Fixed const &rhs) const {
	return !(*this == rhs);
}

bool	Fixed::operator<(Fixed const &rhs) const {
	return (this->toFloat() < rhs.toFloat());
}

bool	Fixed::operator>(Fixed const &rhs) const {
	return (this->toFloat() > rhs.toFloat());
}

bool	Fixed::operator<=(Fixed const &rhs) const {
	return (this->toFloat() <= rhs.toFloat());
}

bool	Fixed::operator>=(Fixed const &rhs) const {
	return (this->toFloat() >= rhs.toFloat());
}

//ARITHMETIC OPERATORS
Fixed Fixed::operator+(Fixed const &rhs) {
	return Fixed(this->toFloat() + rhs.toFloat());
}

Fixed Fixed::operator-(Fixed const &rhs) {
	return Fixed(this->toFloat() - rhs.toFloat());
}

Fixed Fixed::operator*(Fixed const &rhs) {
	return Fixed(this->toFloat() * rhs.toFloat());
}

Fixed Fixed::operator/(Fixed const &rhs) {
	return Fixed(this->toFloat() / rhs.toFloat());
}

Fixed &	Fixed::operator=(Fixed const &rhs) {
	if (this != &rhs)
		this->_fixedPoint = rhs.getRawBits(); // equivalent to use setRawBits
	return *this;
}

Fixed& Fixed::operator+=(Fixed const &rhs) {
	this->setRawBits(this->toFloat() + rhs.toFloat());
	return *this;
}

Fixed& Fixed::operator-=(Fixed const &rhs) {
	this->setRawBits(this->toFloat() - rhs.toFloat());
	return *this;
}

Fixed& Fixed::operator*=(Fixed const &rhs) {
	this->setRawBits(this->toFloat() * rhs.toFloat());
	return *this;
}

Fixed& Fixed::operator/=(Fixed const &rhs) {
	this->setRawBits(this->toFloat() / rhs.toFloat());
	return *this;
}

//POST-PRE IN(DE)CREMENT

Fixed& Fixed::operator++() {
	// Fixed temp(this->toFloat() + 1);
	// this->_fixedPoint = temp.getRawBits();
	this->_fixedPoint++;
	return *this;
}

Fixed Fixed::operator++(int) {
	Fixed temp(this->toFloat());
	++(*this);
	return temp;
}

Fixed& Fixed::operator--() {
	// Fixed temp(this->toFloat() - 1);
	// this->_fixedPoint = temp.getRawBits();
	this->_fixedPoint--;
	return *this;
}

Fixed Fixed::operator--(int) {
	Fixed temp(this->toFloat());
	--(*this);
	return temp;
}
