# CPP Resume

```cpp
#ifndef CLASS_HPP
# define CLASS_HPP

#include <iostream> // inclut string
#include "ParentClass.hpp"

class Class : public ParentClass
{
	private:
		int _int;
		std::string _string;
		static int _staticVar; // Initialization in CPP /!\
		Class(void);
	protected:

	public:
		Class(Class const &src);
		virtual ~Class(void);

		static int	staticFonction( void ); // Methode static pour attributs Static /!\
		virtual void fonctionPure(); // Methode from ParentClass
		Class &	operator=(Class const &rhs);

		class ExceptionName : public std::exception {
			public :
				const char* what() const throw() {
					return "Message To Throw"; }
		};
};

std::ostream &	operator<<(std::ostream &o, Class const &rhs);

#endif
```

## Random
```cpp
srand(time(0)); // Call Once
std::rand() // Call Infinity Random Number !
```

## Heritage en Diamant
![HeritageDiamant](CPP_Piscine/ressources/DiamondHeritage.png)


## Tableau de Pointeurs sur Fonctions [[here](CPP_Piscine/d01/ex09/Logger.cpp)]
```cpp
// EXAMPLE /!\
typedef void (Class::*tabPtr)(std::string const & msg); // Parameter he takes

struct arrayFun{
	tabPtr fn;
	const std::string key;
};
	arrayFun nameOfArray[] =
		{
			{ &Class::nameFunction1, "string1"},
			{ &Class::nameFunction2, "string2"},
		};
		
	int len = (sizeof(nameOfArray)/sizeof(*nameOfArray));
	for (int i = 0; i < len; i++) {
		if (!stringToCompare.compare(nameOfArray[i].key))
		{
			(this->*nameOfArray[i].fn)(nameFunction(msg));
			return;
		}
	}
```


## Diagrams Containers 
![Containers](CPP_Piscine/ressources/Containers.png)

## Cast
|       Cast       | Conv. | Reint. | Upcast | Downcast | Type qual. |
|------------------|:-----:|:------:|:------:|:--------:|:----------:|
| Implicit         |**Yes**|        |**Yes** |          |            |
| static_cast      |**Yes**|        |**Yes** |**Yes**   |            |
| dynamic_cast     |       |        |**Yes** |**Yes**   |            |
| const_cast       |       |        |        |          |**Yes**     |
| reinterpret_cast |       |**Yes** |**Yes** |**Yes**   |            |
| -Legacy C cast-  |**Yes**|**Yes** |**Yes** |**Yes**   |**Yes**     |


|       Cast       | Semantic check | Reliable at run | Tested at run |
|------------------|:--------------:|:---------------:|:-------------:|
| Implicit         |**Yes**         |**Yes**          |               |
| static_cast      |**Yes**         |                 |               |
| dynamic_cast     |**Yes**         |**Yes**          |**Yes**        |
| const_cast       |                |                 |               |
| reinterpret_cast |                |                 |               |
| Legacy C cast    |                |                 |               |

```diff
+ this will be highlighted in green
- this will be highlighted in red
```



























