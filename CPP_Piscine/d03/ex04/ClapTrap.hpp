#ifndef CLAPTRAP_CLASS_HPP
# define CLAPTRAP_CLASS_HPP

#include <iostream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <random>

class ClapTrap
{
	private:
	protected:
		static bool _runSrandOnce;

		unsigned int _hitPoints;
		unsigned int _maxPoints;
		unsigned int _energyPoints;
		unsigned int _maxEnergy;
		unsigned int _level;
		std::string _name;

		unsigned int _mAttackDmg;
		unsigned int _rAttackDmg;
		unsigned int _armorReduc;

	public:
		virtual void announce() const ;
		virtual void meleeAttack(std::string const &) const ;
		virtual void rangedAttack(std::string const &) const ;
		void takeDamage(unsigned int);
		void beRepaired(unsigned int);

		ClapTrap();
		ClapTrap(int, unsigned int const, int, unsigned int const, unsigned int, std::string, unsigned int, unsigned int, unsigned int);
		ClapTrap(ClapTrap const &src);
		~ClapTrap(void);

		ClapTrap &	operator=(ClapTrap const &rhs);

		void getInfos() const ;
		std::string getName() const;
		unsigned int getHitPoint() const;
		unsigned int getMaxPoint() const;
		unsigned int getEnergy() const;
		unsigned int getMaxEnergy() const;
		unsigned int getLevel() const;
		unsigned int getMAttack() const;
		unsigned int getRAttack() const;
		unsigned int getArmor() const;
};

//std::ostream &	operator<<(std::ostream &o, ClapTrap const &rhs);

#endif
