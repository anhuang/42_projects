#include "FragTrap.hpp"

FragTrap::FragTrap(void) : ClapTrap(100, 100, 100, 100, 1, "Default", 30, 20, 5)
{
	announce();
//	std::cout << " FragTrap Default Constructor " << std::endl;
	return;
}

FragTrap::FragTrap(std::string name) : ClapTrap(100, 100, 100, 100, 1, name, 30, 20, 5)
{
	announce();
	// std::cout << " FragTrap Default Constructor " << std::endl;
	return;
}

void FragTrap::announce() const
{
	std::string spawn[] = {
		"Hey everybody! Check out my package!",
		"Let's get this party started!",
		"Glitching weirdness is a term of endearment, right?",
		"Recompiling my combat code!",
		"This time it'll be awesome, I promise!",
		"Look out everybody! Things are about to get awesome!"
	};

	std::string die[] = {
		"Are you god? Am I dead?",
		"I'M DEAD I'M DEAD OHMYGOD I'M DEAD!",
		"AND THEN I'LL DIE! HELP ME! HELP MEEEEE HEE HEE HEEE! HHHHHHHELP!"
	};
	if (this->_hitPoints <= 0)
		std::cout << "[DEAD] " << this->_name << ": " << die[std::rand() % 3] << std::endl;
	else
		std::cout << "[SPAWN] " << this->_name << ": " << spawn[std::rand() % 6] << std::endl;
}

FragTrap::FragTrap(FragTrap const &src)
{
	// std::cout << " FragTrap Copy Constructor " << std::endl;
	*this = src;
	return;
}

FragTrap::~FragTrap(void)
{
	this->_hitPoints = 0;
	announce();
	// std::cout << " FragTrap Destructor " << std::endl;
	return;
}

FragTrap &	FragTrap::operator=(FragTrap const &rhs)
{
	// std::cout << " FragTrap Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxPoints = rhs.getMaxPoint();
		this->_energyPoints = rhs.getEnergy();
		this->_maxEnergy = rhs.getMaxEnergy();
		this->_level = rhs.getLevel();
		this->_mAttackDmg = rhs.getMAttack();
		this->_rAttackDmg = rhs.getRAttack();
		this->_armorReduc = rhs.getArmor();
	}
	return *this;
}

void FragTrap::meleeAttack(std::string const & target) const {
	std::cout << "[MELEE_ATTACK] FR4G-TP " << this->_name << " attacks " << target << " in melee, causing " << this->_mAttackDmg << " points of damage ! "<< std::endl;
}

void FragTrap::rangedAttack(std::string const & target) const {
	std::cout << "[RANGED_ATTACK] FR4G-TP " << this->_name << " attacks " << target << " at range, causing " << this->_rAttackDmg << " points of damage ! "<< std::endl;
}

void FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	if (this->_energyPoints < 25) { std::cout << "Not enough Energy MOUHAHAHAHA :)" << std::endl;
		return; }

	_energyPoints -= 25;
	arrayFun tableOfFunction[] =
		{
			{ &FragTrap::shockElement},
			{ &FragTrap::corrosiveElement},
			{ &FragTrap::cryoElement},
			{ &FragTrap::explosiveElement},
			{ &FragTrap::fireElement}
		};

	if (!FragTrap::_runSrandOnce) {
		FragTrap::_runSrandOnce = true;
		srand(time(NULL));
	}
	(this->*tableOfFunction[std::rand() % 5].fn)(target);
}

void FragTrap::shockElement(std::string const & target) {
	std::cout << "[SPECIAL_ATTACK] " << this->_name << ": Da, da da da! It's electrizing " << target << std::endl;
}
void FragTrap::corrosiveElement(std::string const & target) {
	std::cout << "[SPECIAL_ATTACK] " << this->_name << " is a mean, green, acid machine attack " << target << std::endl;
}
void FragTrap::cryoElement(std::string const & target) {
	std::cout << "[SPECIAL_ATTACK] " << this->_name << "(Attacker) is Ice to meet you " << target << "(attacked)" << std::endl;
}
void FragTrap::explosiveElement(std::string const & target) {
	std::cout << "[SPECIAL_ATTACK] " << this->_name <<  " make Things are exploded and... stuff of " << target << std::endl;
}
void FragTrap::fireElement(std::string const & target) {
	std::cout << "[SPECIAL_ATTACK] " << target <<  " Give your princesses to " << this->_name << ". Burn, baby, burn!" << std::endl;
}

bool FragTrap::_runSrandOnce = false;
