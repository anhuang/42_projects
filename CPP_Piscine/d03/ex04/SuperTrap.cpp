#include "SuperTrap.hpp"

SuperTrap::SuperTrap(void) : ClapTrap(60, 60, 120, 120, 1, "Default", 60, 5, 0)
{
	announce();
}

SuperTrap::SuperTrap(std::string name) : ClapTrap(100, 100, 120, 120, 1, name, 60, 15, 5)
{
	announce();
}

void SuperTrap::announce() const
{
	std::string spawn[] = {
		"This time it'll be awesome, I promise!",
		"Hey everybody, check out my package!",
		"Place your bets!",
		"Defragmenting!",
		"Recompiling my combat code!",
		"Running the sequencer!"
	};

	std::string die[] = {
		"I AM ON FIRE!!! OH GOD, PUT ME OUT!!!",
		"Ratattattattatta! Powpowpowpow! Powpowpowpow! Pew-pew, pew-pew-pewpew!",
		"Don't forget me!"
	};
	if (this->_hitPoints <= 0)
		std::cout << "[DEAD] " << this->_name << ": " << die[std::rand() % 3] << std::endl;
	else
		std::cout << "[SPAWN] " << this->_name << ": " << spawn[std::rand() % 6] << std::endl;
}

SuperTrap::SuperTrap(SuperTrap const &src)
{
	*this = src;
}

SuperTrap::~SuperTrap(void)
{
	this->_hitPoints = 0;
	announce();
}

SuperTrap &	SuperTrap::operator=(SuperTrap const &rhs)
{
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxPoints = rhs.getMaxPoint();
		this->_energyPoints = rhs.getEnergy();
		this->_maxEnergy = rhs.getMaxEnergy();
		this->_level = rhs.getLevel();
		this->_mAttackDmg = rhs.getMAttack();
		this->_rAttackDmg = rhs.getRAttack();
		this->_armorReduc = rhs.getArmor();
	}
	return *this;
}

void SuperTrap::meleeAttack(std::string const & target) const {
	NinjaTrap::meleeAttack(target);
}

void SuperTrap::rangedAttack(std::string const & target) const {
	FragTrap::rangedAttack(target);
}

void SuperTrap::ninjaShoeBox(ClapTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " CLAP YOUR HANDS, CLAPCLAPCLAPCLAP " << target.getName() << " in BOXES !" << std::endl;
}
void SuperTrap::ninjaShoeBox(FragTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " is a new Fragmentation of Fragamenter by " << target.getName() << std::endl;
}
void SuperTrap::ninjaShoeBox(ScavTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " Scalar is not Scalable by " << target.getName() << std::endl;
}
void SuperTrap::ninjaShoeBox(SuperTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " NINJA LEGO : ADVENTURE " << target.getName() << " JOJO." << std::endl;
}

bool SuperTrap::_runSrandOnce = false;
