#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap
{
	private:
		static bool _runSrandOnce;
	protected:


	public:
		ScavTrap(void);
		ScavTrap(std::string );
		ScavTrap(ScavTrap const &);
		~ScavTrap(void);

		virtual void announce() const;

		virtual void meleeAttack(std::string const &) const ;
		virtual void rangedAttack(std::string const &) const ;

		void challengeNewcomer(std::string const &);

		void iceBucket(std::string const &);
		void hotPepper(std::string const &);
		void chubbyBunny(std::string const &);
		void saltBae(std::string const &);
		void whisper(std::string const &);

		std::string getName() const ;

		ScavTrap &	operator=(ScavTrap const &rhs);
};

// std::ostream &	operator<<(std::ostream &o, ScavTrap const &rhs);
typedef void (ScavTrap::*tabPtrScav)(std::string const & target);

struct arrayFunScav{
	tabPtrScav fn;
};

#endif
