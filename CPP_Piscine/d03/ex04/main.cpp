#include "SuperTrap.hpp"

int main(void)
{
	FragTrap andrew("Andrew");
	ScavTrap isa("Isabelle");
	ClapTrap ella = ClapTrap(100, 100, 100, 100, 1, "ella", 30, 20, 5);
	SuperTrap ernest("Ernest");

	ernest.meleeAttack(andrew.getName());
	ernest.rangedAttack(andrew.getName());
	return (0);
}
