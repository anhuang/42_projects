#ifndef SUPERTRAP_HPP
# define SUPERTRAP_HPP

#include "FragTrap.hpp"
#include "NinjaTrap.hpp"


class SuperTrap : public virtual FragTrap, public virtual NinjaTrap
{
	private:
		static bool _runSrandOnce;
	protected:


	public:
		SuperTrap(void);
		SuperTrap(std::string );
		SuperTrap(SuperTrap const &);
		~SuperTrap(void);

		virtual void announce() const;

		virtual void meleeAttack(std::string const &) const ;
		virtual void rangedAttack(std::string const &) const ;

		void challengeNewcomer(std::string const &);

		void iceBucket(std::string const &);
		void hotPepper(std::string const &);
		void chubbyBunny(std::string const &);
		void saltBae(std::string const &);
		void whisper(std::string const &);

		void ninjaShoeBox(ClapTrap const &);
		void ninjaShoeBox(ScavTrap const &);
		void ninjaShoeBox(FragTrap const &);
		void ninjaShoeBox(SuperTrap const &);

		SuperTrap &	operator=(SuperTrap const &rhs);
};

// std::ostream &	operator<<(std::ostream &o, SuperTrap const &rhs);

#endif
