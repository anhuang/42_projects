#include "ClapTrap.hpp"

ClapTrap::ClapTrap() : _hitPoints(100), _maxPoints(100), _energyPoints(100), _maxEnergy(100), _level(1), _name("Default"), _mAttackDmg(20), _rAttackDmg(10), _armorReduc(5)
{
	announce();
}

ClapTrap::ClapTrap(int hitPoints, unsigned int maxPoints, int energyPoints, unsigned int maxEnergy, unsigned int level, std::string name, unsigned int mAttackDmg, unsigned int rAttackDmg, unsigned int armorReduc) :
			_hitPoints(hitPoints), _maxPoints(maxPoints), _energyPoints(energyPoints), _maxEnergy(maxEnergy), _level(level), _name(name), _mAttackDmg(mAttackDmg), _rAttackDmg(rAttackDmg), _armorReduc(armorReduc)
{
	announce();
	// std::cout << " ClapTrap Default Constructor " << std::endl;
	return;
}

ClapTrap::ClapTrap(ClapTrap const &src) : _maxPoints(src._maxPoints) ,_maxEnergy(src._maxEnergy)
{
	// std::cout << " ClapTrap Copy Constructor " << std::endl;
	*this = src;
	return;
}

ClapTrap::~ClapTrap(void)
{
	_hitPoints = 0;
	announce();
	return;
}

ClapTrap &	ClapTrap::operator=(ClapTrap const &rhs)
{
	// std::cout << " ClapTrap Assignement Operator " << std::endl;

	if (this != &rhs) {
		_name = rhs.getName();
		_hitPoints = rhs.getHitPoint();
		_maxPoints = rhs.getMaxPoint();
		_energyPoints = rhs.getEnergy();
		_maxEnergy = rhs.getMaxEnergy();
		_level = rhs.getLevel();
		_mAttackDmg = rhs.getMAttack();
		_rAttackDmg = rhs.getRAttack();
		_armorReduc = rhs.getArmor();
	}

	return *this;
}

void ClapTrap::announce() const
{
	if (_hitPoints <= 0)
		std::cout << "[DEAD] " << _name << ": DEPOP !" << std::endl;
	else
		std::cout << "[SPAWN] " << _name << ": SPAWNED !" << std::endl;
}

void ClapTrap::meleeAttack(std::string const & target) const {
	std::cout << "[MELEE_ATTACK] " << _name << " attacks " << target << " in melee, causing " << _mAttackDmg << " points of damage ! "<< std::endl;
}

void ClapTrap::rangedAttack(std::string const & target) const {
	std::cout << "[RANGED_ATTACK] " << _name << " attacks " << target << " at range, causing " << _rAttackDmg << " points of damage ! "<< std::endl;
}

void ClapTrap::takeDamage(unsigned int amount) {
	if (_armorReduc >= amount){
		std::cout << "[TAKE_DAMAGE] Not affect to me HAHAHA SHIELD POWAAA" << std::endl;
		return; }
	std::cout << "[TAKE_DAMAGE] Ouch crouch " << _name << " have been attacked... " << amount - _armorReduc << " of Damage." << std::endl;
	_hitPoints = (amount - _armorReduc) > _hitPoints ? 0 : _hitPoints - (amount - _armorReduc);
	if (_hitPoints == 0)
		announce();
	else
		std::cout << _name << " is now at " << _hitPoints << " Hit Points." << std::endl;
}

void ClapTrap::beRepaired(unsigned int amount) { // Utiliser Clamp serait mieux !
	_hitPoints += amount;
	_hitPoints = _hitPoints > ClapTrap::_maxPoints ? ClapTrap::_maxPoints : _hitPoints;
	std::cout << "[HEAL] I found health! " << amount << " HitPoints in more, I'm now at " << _hitPoints << std::endl;
}

void ClapTrap::getInfos() const {
	std::cout << "Name : " << _name << std::endl;
	std::cout << "HitPoints : " << _hitPoints << "/" << _maxPoints << std::endl;
	std::cout << "Energy : " << _energyPoints << "/" << _maxEnergy << std::endl;
	std::cout << "Level : " << _level << std::endl;
	std::cout << "Melee Dmg : " << _mAttackDmg << std::endl;
	std::cout << "Ranged Dmg : " << _rAttackDmg << std::endl;
}

std::string ClapTrap::getName() const { return _name; }
unsigned int ClapTrap::getHitPoint() const {return _hitPoints; }
unsigned int ClapTrap::getMaxPoint() const {return _maxPoints; }
unsigned int ClapTrap::getEnergy() const {return _energyPoints; }
unsigned int ClapTrap::getMaxEnergy() const {return _maxEnergy; }
unsigned int ClapTrap::getLevel() const {return _level; }
unsigned int ClapTrap::getMAttack() const {return _mAttackDmg; }
unsigned int ClapTrap::getRAttack() const {return _rAttackDmg; }
unsigned int ClapTrap::getArmor() const {return _armorReduc; }
