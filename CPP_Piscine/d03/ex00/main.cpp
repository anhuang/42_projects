#include "FragTrap.hpp"

int main(void)
{
	FragTrap andrew("Andrew");
	FragTrap isa("Isabelle");
	andrew.beRepaired(50);
	andrew.takeDamage(60);
	andrew.beRepaired(50);
	andrew.meleeAttack("Ella");
	andrew.rangedAttack("Timo");
	andrew.vaulthunter_dot_exe("Enemy1");
	andrew.vaulthunter_dot_exe("Enemy2");
	andrew.vaulthunter_dot_exe("Enemy3");
	andrew.vaulthunter_dot_exe("Enemy4");
	andrew.vaulthunter_dot_exe("Enemy5");

	andrew = isa;
	std::cout << "JE M'APELLE " << andrew.getName() << std::endl;
	return (0);
}
