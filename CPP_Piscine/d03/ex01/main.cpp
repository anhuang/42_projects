#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main(void)
{
	FragTrap andrew("Andrew");
	FragTrap ella("Ella");
	ScavTrap isa("Isabelle");

	std::cout << "###FRAG TRAP CALLED " << andrew.getName() << "###" << std::endl;
	andrew.beRepaired(50);
	andrew.takeDamage(60);
	andrew.beRepaired(50);
	andrew.meleeAttack("Ella");
	andrew.rangedAttack("Timo");
	andrew.vaulthunter_dot_exe("Enemy1");
	andrew.vaulthunter_dot_exe("Enemy2");
	andrew.vaulthunter_dot_exe("Enemy3");
	andrew.vaulthunter_dot_exe("Enemy4");
	andrew.vaulthunter_dot_exe("Enemy5");
	andrew.takeDamage(200);

	andrew = ella;
	std::cout << "andrew : " << andrew.getName() << std::endl;

	std::cout << "\n###SCAV TRAP CALLED " << isa.getName() << "###" << std::endl;
	isa.takeDamage(40);
	isa.takeDamage(3);
	isa.challengeNewcomer(andrew.getName());
	isa.challengeNewcomer(andrew.getName());
	isa.challengeNewcomer(andrew.getName());
	isa.takeDamage(66);
	return (0);
}
