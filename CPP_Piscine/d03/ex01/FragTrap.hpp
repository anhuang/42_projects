#ifndef FRAGTRAP_CLASS_HPP
# define FRAGTRAP_CLASS_HPP

#include <iostream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>

class FragTrap
{
	private:
		static bool _runSrandOnce;

		int _hitPoints;
		int _maxPoints;
		int _energyPoints;
		int _maxEnergy;
		int _level;
		std::string _name;

		int _mAttackDmg;
		int _rAttackDmg;
		unsigned int _armorReduc;
	protected:


	public:
		FragTrap(void);
		FragTrap(std::string );
		FragTrap(FragTrap const &);
		~FragTrap(void);

		void announce();
		void meleeAttack(std::string const &);
		void rangedAttack(std::string const &);
		void takeDamage(unsigned int);
		void beRepaired(unsigned int);

		void vaulthunter_dot_exe(std::string const &);

		void shockElement(std::string const &);
		void corrosiveElement(std::string const &);
		void cryoElement(std::string const &);
		void explosiveElement(std::string const &);
		void fireElement(std::string const &);

		FragTrap &	operator=(FragTrap const &rhs);

		std::string getName() const;
		unsigned int getHitPoint() const;
		unsigned int getMaxPoint() const;
		unsigned int getEnergy() const;
		unsigned int getMaxEnergy() const;
		unsigned int getLevel() const;
		unsigned int getMAttack() const;
		unsigned int getRAttack() const;
		unsigned int getArmor() const;
};

// std::ostream &	operator<<(std::ostream &o, FragTrap const &rhs);
typedef void (FragTrap::*tabPtr)(std::string const & target);

struct arrayFun{
	tabPtr fn;
};

#endif
