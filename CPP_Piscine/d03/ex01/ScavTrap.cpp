#include "ScavTrap.hpp"

ScavTrap::ScavTrap(void) : _hitPoints(100), _maxPoints(100), _energyPoints(50), _maxEnergy(50), \
	_level(1), _name("Default"), _mAttackDmg(20), _rAttackDmg(15), _armorReduc(3)
{
	announce();
//	std::cout << " ScavTrap Default Constructor " << std::endl;
	return;
}

ScavTrap::ScavTrap(std::string name) : _hitPoints(100), _maxPoints(100), _energyPoints(50), _maxEnergy(50), \
	_level(1), _name(name), _mAttackDmg(20), _rAttackDmg(15), _armorReduc(3)
{
	announce();
//	std::cout << " ScavTrap Default Constructor " << std::endl;
	return;
}

void ScavTrap::announce()
{
	std::string spawn[] = {
		"This time it'll be awesome, I promise!",
		"Hey everybody, check out my package!",
		"Place your bets!",
		"Defragmenting!",
		"Recompiling my combat code!",
		"Running the sequencer!"
	};

	std::string die[] = {
		"I AM ON FIRE!!! OH GOD, PUT ME OUT!!!",
		"Ratattattattatta! Powpowpowpow! Powpowpowpow! Pew-pew, pew-pew-pewpew!",
		"Don't forget me!"
	};
	if (this->_hitPoints <= 0)
		std::cout << "[DEAD] " << this->_name << ": " << die[std::rand() % 3] << std::endl;
	else
		std::cout << "[SPAWN] " << this->_name << ": " << spawn[std::rand() % 6] << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &src)
{
//	std::cout << " ScavTrap Copy Constructor " << std::endl;
	*this = src;
	return;
}

ScavTrap::~ScavTrap(void)
{
	this->_hitPoints = 0;
	announce();
//	std::cout << " ScavTrap Destructor " << std::endl;
	return;
}

ScavTrap &	ScavTrap::operator=(ScavTrap const &rhs)
{
	std::cout << " ScavTrap Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxPoints = rhs.getMaxPoint();
		this->_energyPoints = rhs.getEnergy();
		this->_maxEnergy = rhs.getMaxEnergy();
		this->_level = rhs.getLevel();
		this->_mAttackDmg = rhs.getMAttack();
		this->_rAttackDmg = rhs.getRAttack();
		this->_armorReduc = rhs.getArmor();
	}

	return *this;
}

void ScavTrap::meleeAttack(std::string const & target) {
	std::cout << "[MELEE_ATTACK] SC4V-TP " << this->_name << " attacks " << target << " in melee, causing " << this->_mAttackDmg << " points of damage ! "<< std::endl;
}

void ScavTrap::rangedAttack(std::string const & target) {
	std::cout << "[RANGED_ATTACK] SC4V-TP " << this->_name << " attacks " << target << " at range, causing " << this->_rAttackDmg << " points of damage ! "<< std::endl;
}

void ScavTrap::takeDamage(unsigned int amount) {
	if (this->_armorReduc >= amount){
		std::cout << "[TAKE_DAMAGE] Not affect to me HAHAHA SHIELD POWAAA" << std::endl;
		return; }
	std::cout << "[TAKE_DAMAGE] Ouch crouch " << this->_name << " have been attacked... " << amount - this->_armorReduc << " of Damage." << std::endl;
	this->_hitPoints -= (amount - this->_armorReduc);
	this->_hitPoints = this->_hitPoints < 0 ? 0 : this->_hitPoints;
	if (this->_hitPoints == 0)
		announce();
	std::cout << this->_name << " is now at " << this->_hitPoints << " Hit Points." << std::endl;
}

void ScavTrap::beRepaired(unsigned int amount) { // Utiliser Clamp serait mieux !
	this->_hitPoints += amount;
	this->_hitPoints = this->_hitPoints > ScavTrap::_maxPoints ? ScavTrap::_maxPoints : this->_hitPoints;
	std::cout << "[HEAL] I found health! " << amount << " HitPoints in more, I'm now at " << this->_hitPoints << std::endl;
}

void ScavTrap::challengeNewcomer(std::string const & target)
{
	if (this->_energyPoints < 25) { std::cout << "Not enough Energy MOUHAHAHAHA :)" << std::endl;
		return; }

	arrayFunScav tableOfFunction[] =
		{
			{ &ScavTrap::iceBucket},
			{ &ScavTrap::hotPepper},
			{ &ScavTrap::chubbyBunny},
			{ &ScavTrap::saltBae},
			{ &ScavTrap::whisper}
		};

	if (!ScavTrap::_runSrandOnce) {
	ScavTrap::_runSrandOnce = true;
	srand(time(NULL));
	}

	(this->*tableOfFunction[std::rand() % 5].fn)(target);
}

void ScavTrap::iceBucket(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in IceBucketChallenge !" << std::endl;
}
void ScavTrap::hotPepper(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in HotPepperChallenge !" << std::endl;
}
void ScavTrap::chubbyBunny(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in ChubbyBunnyChallenge !" << std::endl;
}
void ScavTrap::saltBae(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in SaltBaeChallenge !" << std::endl;
}
void ScavTrap::whisper(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in hotPepperChallenge !" << std::endl;
}

std::string ScavTrap::getName() const { return this->_name; }
unsigned int ScavTrap::getHitPoint() const {return this->_hitPoints; }
unsigned int ScavTrap::getMaxPoint() const {return this->_maxPoints; }
unsigned int ScavTrap::getEnergy() const {return this->_energyPoints; }
unsigned int ScavTrap::getMaxEnergy() const {return this->_maxEnergy; }
unsigned int ScavTrap::getLevel() const {return this->_level; }
unsigned int ScavTrap::getMAttack() const {return this->_mAttackDmg; }
unsigned int ScavTrap::getRAttack() const {return this->_rAttackDmg; }
unsigned int ScavTrap::getArmor() const {return this->_armorReduc; }

bool ScavTrap::_runSrandOnce = false;
