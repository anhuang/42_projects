#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include <iostream>
#include <string>
#include <stdlib.h>
#include <ctime>
#include <stdio.h>
#include <random>

class ScavTrap
{
	private:
		static bool _runSrandOnce;

		int _hitPoints;
		int _maxPoints;
		int _energyPoints;
		int _maxEnergy;
		int _level;
		std::string _name;

		int _mAttackDmg;
		int _rAttackDmg;
		unsigned int _armorReduc;
	protected:


	public:
		ScavTrap(void);
		ScavTrap(std::string );
		ScavTrap(ScavTrap const &);
		~ScavTrap(void);

		void announce();
		void meleeAttack(std::string const &);
		void rangedAttack(std::string const &);
		void takeDamage(unsigned int);
		void beRepaired(unsigned int);

		void challengeNewcomer(std::string const &);

		void iceBucket(std::string const &);
		void hotPepper(std::string const &);
		void chubbyBunny(std::string const &);
		void saltBae(std::string const &);
		void whisper(std::string const &);

		std::string getName() const;
		unsigned int getHitPoint() const;
		unsigned int getMaxPoint() const;
		unsigned int getEnergy() const;
		unsigned int getMaxEnergy() const;
		unsigned int getLevel() const;
		unsigned int getMAttack() const;
		unsigned int getRAttack() const;
		unsigned int getArmor() const;

		ScavTrap &	operator=(ScavTrap const &rhs);
};

// std::ostream &	operator<<(std::ostream &o, ScavTrap const &rhs);
typedef void (ScavTrap::*tabPtrScav)(std::string const & target);

struct arrayFunScav{
	tabPtrScav fn;
};

#endif
