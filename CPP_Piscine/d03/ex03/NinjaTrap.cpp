#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap(void) : ClapTrap(60, 60, 120, 120, 1, "Default", 60, 5, 0)
{
	announce();
}

NinjaTrap::NinjaTrap(std::string name) : ClapTrap(60, 60, 120, 120, 1, name, 60, 5, 0)
{
	announce();
}

void NinjaTrap::announce() const
{
	std::string spawn[] = {
		"This time it'll be awesome, I promise!",
		"Hey everybody, check out my package!",
		"Place your bets!",
		"Defragmenting!",
		"Recompiling my combat code!",
		"Running the sequencer!"
	};

	std::string die[] = {
		"I AM ON FIRE!!! OH GOD, PUT ME OUT!!!",
		"Ratattattattatta! Powpowpowpow! Powpowpowpow! Pew-pew, pew-pew-pewpew!",
		"Don't forget me!"
	};
	if (this->_hitPoints <= 0)
		std::cout << "[DEAD] " << this->_name << ": " << die[std::rand() % 3] << std::endl;
	else
		std::cout << "[SPAWN] " << this->_name << ": " << spawn[std::rand() % 6] << std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const &src)
{
	*this = src;
}

NinjaTrap::~NinjaTrap(void)
{
	this->_hitPoints = 0;
	announce();
}

NinjaTrap &	NinjaTrap::operator=(NinjaTrap const &rhs)
{
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxPoints = rhs.getMaxPoint();
		this->_energyPoints = rhs.getEnergy();
		this->_maxEnergy = rhs.getMaxEnergy();
		this->_level = rhs.getLevel();
		this->_mAttackDmg = rhs.getMAttack();
		this->_rAttackDmg = rhs.getRAttack();
		this->_armorReduc = rhs.getArmor();
	}
	return *this;
}

void NinjaTrap::meleeAttack(std::string const & target) const {
	std::cout << "[MELEE_ATTACK] NINJA-TP " << this->_name << " attacks " << target << " in melee, causing " << this->_mAttackDmg << " points of damage ! "<< std::endl;
}

void NinjaTrap::rangedAttack(std::string const & target) const {
	std::cout << "[RANGED_ATTACK] NINJA-TP " << this->_name << " attacks " << target << " at range, causing " << this->_rAttackDmg << " points of damage ! "<< std::endl;
}

void NinjaTrap::ninjaShoeBox(ClapTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " CLAP YOUR HANDS, CLAPCLAPCLAPCLAP " << target.getName() << " in BOXES !" << std::endl;
}
void NinjaTrap::ninjaShoeBox(FragTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " is a new Fragmentation of Fragamenter by " << target.getName() << std::endl;
}
void NinjaTrap::ninjaShoeBox(ScavTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " Scalar is not Scalable by " << target.getName() << std::endl;
}
void NinjaTrap::ninjaShoeBox(NinjaTrap const & target) {
	std::cout << "{Ninja Shoe Box} " << this->_name << " NINJA LEGO : ADVENTURE " << target.getName() << " JOJO." << std::endl;
}

bool NinjaTrap::_runSrandOnce = false;
