#ifndef FRAGTRAP_CLASS_HPP
# define FRAGTRAP_CLASS_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap
{
	private:
		static bool _runSrandOnce;
		//
		// int _hitPoints;
		// int static const _maxPoints;
		// int _energyPoints;
		// int static const _maxEnergy;
		// int _level;
		// std::string _name;
		//
		// int _mAttackDmg;
		// int _rAttackDmg;
		// unsigned int _armorReduc;
	protected:


	public:
		FragTrap(void);
		FragTrap(std::string );
		FragTrap(FragTrap const &);
		~FragTrap(void);

		virtual void announce() const;
		virtual void meleeAttack(std::string const &) const ;
		virtual void rangedAttack(std::string const &) const ;
		// void takeDamage(unsigned int);
		// void beRepaired(unsigned int);

		void vaulthunter_dot_exe(std::string const &);

		void shockElement(std::string const &);
		void corrosiveElement(std::string const &);
		void cryoElement(std::string const &);
		void explosiveElement(std::string const &);
		void fireElement(std::string const &);

		FragTrap &	operator=(FragTrap const &rhs);
};

// std::ostream &	operator<<(std::ostream &o, FragTrap const &rhs);
typedef void (FragTrap::*tabPtr)(std::string const & target);

struct arrayFun{
	tabPtr fn;
};

#endif
