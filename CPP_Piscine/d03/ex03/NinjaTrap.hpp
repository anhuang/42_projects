#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

#include "FragTrap.hpp"
#include "ScavTrap.hpp"


class NinjaTrap : public ClapTrap
{
	private:
		static bool _runSrandOnce;
	protected:


	public:
		NinjaTrap(void);
		NinjaTrap(std::string );
		NinjaTrap(NinjaTrap const &);
		~NinjaTrap(void);

		virtual void announce() const;

		virtual void meleeAttack(std::string const &) const ;
		virtual void rangedAttack(std::string const &) const ;

		void challengeNewcomer(std::string const &);

		void iceBucket(std::string const &);
		void hotPepper(std::string const &);
		void chubbyBunny(std::string const &);
		void saltBae(std::string const &);
		void whisper(std::string const &);

		void ninjaShoeBox(ClapTrap const &);
		void ninjaShoeBox(ScavTrap const &);
		void ninjaShoeBox(FragTrap const &);
		void ninjaShoeBox(NinjaTrap const &);

		NinjaTrap &	operator=(NinjaTrap const &rhs);
};

// std::ostream &	operator<<(std::ostream &o, NinjaTrap const &rhs);

#endif
