#include "NinjaTrap.hpp"

int main(void)
{
	FragTrap andrew("Andrew");
	ScavTrap isa("Isabelle");
	ClapTrap ella = ClapTrap(100, 100, 100, 100, 1, "ella", 30, 20, 5);
	NinjaTrap ernest("Ernest");

	ernest.ninjaShoeBox(andrew);
	ernest.ninjaShoeBox(ella);
	ernest.ninjaShoeBox(isa);
	ernest.ninjaShoeBox(ernest);
	return (0);
}
