#include "ScavTrap.hpp"

ScavTrap::ScavTrap(void) : ClapTrap(100, 100, 50, 50, 1, "Default", 20, 15, 3)
{
	announce();
	std::cout << " ScavTrap Default Constructor " << std::endl;
	return;
}

ScavTrap::ScavTrap(std::string name) : ClapTrap(100, 100, 50, 50, 1, name, 20, 15, 3)
{
	announce();
	std::cout << " ScavTrap Default Constructor " << std::endl;
	return;
}

void ScavTrap::announce() const
{
	std::string spawn[] = {
		"This time it'll be awesome, I promise!",
		"Hey everybody, check out my package!",
		"Place your bets!",
		"Defragmenting!",
		"Recompiling my combat code!",
		"Running the sequencer!"
	};

	std::string die[] = {
		"I AM ON FIRE!!! OH GOD, PUT ME OUT!!!",
		"Ratattattattatta! Powpowpowpow! Powpowpowpow! Pew-pew, pew-pew-pewpew!",
		"Don't forget me!"
	};
	if (this->_hitPoints <= 0)
		std::cout << "[DEAD] " << this->_name << ": " << die[std::rand() % 3] << std::endl;
	else
		std::cout << "[SPAWN] " << this->_name << ": " << spawn[std::rand() % 6] << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &src)
{
	std::cout << " ScavTrap Copy Constructor " << std::endl;
	*this = src;
	return;
}

ScavTrap::~ScavTrap(void)
{
	this->_hitPoints = 0;
	announce();
	std::cout << " ScavTrap Destructor " << std::endl;
	return;
}

ScavTrap &	ScavTrap::operator=(ScavTrap const &rhs)
{
	std::cout << " ScavTrap Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoint();
		this->_maxPoints = rhs.getMaxPoint();
		this->_energyPoints = rhs.getEnergy();
		this->_maxEnergy = rhs.getMaxEnergy();
		this->_level = rhs.getLevel();
		this->_mAttackDmg = rhs.getMAttack();
		this->_rAttackDmg = rhs.getRAttack();
		this->_armorReduc = rhs.getArmor();
	}
	return *this;
}

void ScavTrap::meleeAttack(std::string const & target) const {
	std::cout << "[MELEE_ATTACK] SC4V-TP " << this->_name << " attacks " << target << " in melee, causing " << this->_mAttackDmg << " points of damage ! "<< std::endl;
}

void ScavTrap::rangedAttack(std::string const & target) const {
	std::cout << "[RANGED_ATTACK] SC4V-TP " << this->_name << " attacks " << target << " at range, causing " << this->_rAttackDmg << " points of damage ! "<< std::endl;
}

void ScavTrap::challengeNewcomer(std::string const & target)
{
	if (this->_energyPoints < 25) { std::cout << "Not enough Energy MOUHAHAHAHA :)" << std::endl;
		return; }

	arrayFunScav tableOfFunction[] =
		{
			{ &ScavTrap::iceBucket},
			{ &ScavTrap::hotPepper},
			{ &ScavTrap::chubbyBunny},
			{ &ScavTrap::saltBae},
			{ &ScavTrap::whisper}
		};

	if (!ScavTrap::_runSrandOnce) {
		ScavTrap::_runSrandOnce = true;
		srand(time(NULL));
	}

	(this->*tableOfFunction[std::rand() % 5].fn)(target);
}

void ScavTrap::iceBucket(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in IceBucketChallenge !" << std::endl;
}
void ScavTrap::hotPepper(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in HotPepperChallenge !" << std::endl;
}
void ScavTrap::chubbyBunny(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in ChubbyBunnyChallenge !" << std::endl;
}
void ScavTrap::saltBae(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in SaltBaeChallenge !" << std::endl;
}
void ScavTrap::whisper(std::string const & target) {
	std::cout << "[CHALLENGE] " << this->_name << " Challenging " << target << " in hotPepperChallenge !" << std::endl;
}

// std::string ScavTrap:: getName() const { return this->_name; }

bool ScavTrap::_runSrandOnce = false;
