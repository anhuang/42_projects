#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int main(void)
{
	FragTrap andrew("Andrew");
	ScavTrap isa("Isabelle");
	ClapTrap ella = FragTrap();
	andrew.beRepaired(50);
	andrew.takeDamage(60);
	ella.meleeAttack(andrew.getName());
	isa.rangedAttack(ella.getName());
	ella = andrew;
	// std::cout << "###FRAG TRAP CALLED " << andrew.getName() << "###" << std::endl;
	ella.rangedAttack(andrew.getName());
	ella.getInfos();
	ella.takeDamage(100);
	// andrew.beRepaired(50);
	// andrew.meleeAttack("Ella");
	// andrew.rangedAttack("Timo");
	// andrew.vaulthunter_dot_exe("Enemy1");
	// andrew.vaulthunter_dot_exe("Enemy2");
	// andrew.vaulthunter_dot_exe("Enemy3");
	// andrew.vaulthunter_dot_exe("Enemy4");
	// andrew.vaulthunter_dot_exe("Enemy5");
	// andrew.takeDamage(200);
	//
	// std::cout << "\n###SCAV TRAP CALLED " << isa.getName() << "###" << std::endl;
	// isa.takeDamage(40);
	isa.takeDamage(3);
	// isa.challengeNewcomer(andrew.getName());
	// isa.challengeNewcomer(andrew.getName());
	// isa.challengeNewcomer(andrew.getName());
	// isa.takeDamage(66);
	return (0);
}
