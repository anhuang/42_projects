#include "Converter.hpp"

int errorMessage(std::string msg) {
	std::cerr << msg << std::endl;
	return 1;
}

void removeSpaces(std::string & str)
{
	size_t count = 0;

	for (size_t i = 0; str[i]; i++)
		if (!isspace(str[i]))
			str[count++] = static_cast<char>(str[i]);
	str = str.substr(0, count);
}

int countAlpha(std::string s) {
	int res = 0;

	for (size_t i = 0; i < s.length(); i++)
		if (isalpha(s[i]))
			res++;
	return res;
}

char getFirstAlpha(std::string s) {
	for (size_t i = 0; s[i]; i++) {
		if (isalpha(s[i]))
			return s[i];
	}
	return 'f';
}

bool checkAlnum(std::string s) {
	int count = 0;
	for (size_t i = 0; s[i]; i++) {
		if (i == 0 && (s[i] == '+' || s[i] == '-'))
			continue;
		if ((s[i] != 'f' && isalnum(s[i])) && !isdigit(s[i]))
			count++;
	}
	if (count > 1)
		return true;
	return false;
}

void checkToChar(Converter& conv,double nb)
{
	try {
		std::cout << "char: " << conv.toChar(nb) << "'" << std::endl;
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
}
void checkToInt(Converter& conv, double nb)
{
	try {
		std::cout << "int: " << conv.toInt(nb) << std::endl;
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
}
void checkToFloat(Converter& conv, double nb)
{
	try {
		std::cout.precision(1);
		std::cout << "float: " << std::fixed  \
		<< conv.toFloat(nb) << "f" << std::endl;
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
}
void checkToDouble(Converter& conv, double nb)
{
	try {
		std::cout.precision(1);
		std::cout << "double: " << std::fixed \
		<< conv.toDouble(nb) << std::endl;
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
}

int main( int ac, char *av[])
{
	if (ac != 2)
		return errorMessage("Need 1 Number or char : \"int, float, double or char\".");

	std::string str(av[1]);
	removeSpaces(str);
	double test = atof(str.c_str());
	Converter conv;

	std::regex floatReg("^[-+]?[0-9]+(\\.[0-9]+)?f$");
	std::regex doubleReg("^[-+]?[0-9]+(\\.[0-9]+)?$");
	std::regex intReg("^[-+]?[0-9]+$");
	std::regex floatInfNanReg("^([-+]?inf|nan)f$");
	std::regex infNanReg("^([-+]?inf|nan)$");

	if (regex_match(str, floatReg) || regex_match(str, floatInfNanReg)
		|| regex_match(str, doubleReg) || regex_match(str, infNanReg)
		|| regex_match(str, intReg)
		|| str.length() == 1)
	{
		if (str.length() == 1 && !isdigit(str[0]))
			test = str.c_str()[0];
		checkToChar(conv, test);
		checkToInt(conv, test);
		checkToFloat(conv, test);
		checkToDouble(conv, test);
	}
	else
		return errorMessage("Error Input !");
	return (0);
}
