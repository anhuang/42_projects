#ifndef CONVERTER_HPP
# define CONVERTER_HPP

#include <iomanip>
#include <iostream>
#include <string>
#include <limits>
#include <climits>
#include <cfloat>
#include <stdlib.h> // ::atof for the double
#include <math.h>
#include <regex>

class Converter
{
	private:
		Converter(Converter const &src);
		Converter &	operator=(Converter const &rhs);

	protected:


	public:
		Converter(void);
		~Converter(void);

		char toChar(double nb);
		int toInt(double nb);
		float toFloat(double nb);
		double toDouble(double nb);

		class NonDisplayable : public std::exception
		{
			public :
				virtual const char* what() const throw();
		};
		class Impossible : public std::exception
		{
			public :
				virtual const char* what() const throw();
		};
};

//std::ostream &	operator<<(std::ostream &o, Converter const &rhs);

#endif
