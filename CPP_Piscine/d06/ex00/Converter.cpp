#include "Converter.hpp"

Converter::Converter(void) {
}

Converter::Converter(Converter const &src)
{
	*this = src;
}

Converter::~Converter(void)
{
}

Converter &	Converter::operator=(Converter const &rhs)
{
	if (this != &rhs)
		;
//		this->_foo = rhs.getFoo();
	return *this;
}

char Converter::toChar(double nb) {
	if (isinf(nb) || isnan(nb) || nb > 255)
		throw Converter::Impossible();
	char c = static_cast<char>(nb);
	if (!isprint(c) || nb <= 0)
		throw Converter::NonDisplayable();
	std::cout << "'";
	return c;
}

int Converter::toInt(double nb) {
	if (isnan(nb) || isinf(nb) || nb > INT_MAX || nb < INT_MIN)
		throw Converter::Impossible();
	return static_cast<int>(nb);
}

float Converter::toFloat(double nb) {
	return static_cast<float>(nb);
}

double Converter::toDouble(double nb) {
	return static_cast<double>(nb);
}

const char* Converter::NonDisplayable::what() const throw() {
	return "Non displayable";
}

const char* Converter::Impossible::what() const throw() {
	return "impossible";
}
