#include "Base.hpp"

Base * generate(void) {
	int random = std::rand() % 3;

	if (random == 0)
		return new A();
	else if (random == 1)
		return new B();
	else if (random == 2)
		return new C();
	return new A();
}

void identify_from_pointer( Base * p ) {
	A * a = dynamic_cast<A *>(p);
	B * b = dynamic_cast<B *>(p);
	C * c = dynamic_cast<C *>(p);
	if (a)
		std::cout << "A" << std::endl;
	else if (b)
		std::cout << "B" << std::endl;
	else if (c)
		std::cout << "C" << std::endl;
}

void identify_from_reference( Base & p ) {
	try {
		A & a = dynamic_cast<A &>(p);
		std::cout << "A" << std::endl;
		p = a;
		return ;
	}
	catch (std::bad_cast /*&bc*/) {
	}
	try {
		B & b = dynamic_cast<B &>(p);
		std::cout << "B" << std::endl;
		p = b;
		return;
	}
	catch (std::bad_cast /*&bc*/) {
	}
	try {
		C & c = dynamic_cast<C &>(p);
		std::cout << "C" << std::endl;
		p = c;
	}
	catch (std::bad_cast /*&bc*/) {
	}
}

int main(void)
{
	srand(time(0));
	Base* base = generate();
	identify_from_pointer(base);
	identify_from_reference(*base);

	delete base;
	return (0);
}
