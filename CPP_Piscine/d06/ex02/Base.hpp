#ifndef BASE_HPP
# define BASE_HPP

#include <iostream>
#include <string>

class Base
{
	private:

	protected:

	public:
		virtual ~Base(void);

};

class A : public Base{};
class B : public Base{};
class C : public Base{};

//std::ostream &	operator<<(std::ostream &o, Base const &rhs);

#endif
