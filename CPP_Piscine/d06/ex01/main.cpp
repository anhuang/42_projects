#include <string>
#include <iostream>

struct Data {
	std::string s1;
	int n;
	std::string s2;
};

void * serialize( void ) {
	std::string alphanum = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwyz0123456789";
	std::string s1;
	std::string s2;

	for (size_t i = 0; i < 8; i++) {
		s1 = s1 + alphanum[std::rand() % alphanum.length()];
		s2 = s2 + alphanum[std::rand() % alphanum.length()];
	}

	Data * ret = new Data();
	ret->s1 = s1;
	ret->n = std::rand();
	ret->s2 = s2;
	return ret;
}

Data * deserialize( void * raw ) {
	return reinterpret_cast<Data*>(raw);
}

int
main(void)
{
	srand(time(0));
	void           *ser = serialize();
	Data           *des = deserialize(ser);

	std::cout << des->s1 << std::endl \
				<< des->n << std::endl \
				<< des->s2 << std::endl;
	delete des;
	return (0);
}
