/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anhuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 22:28:35 by anhuang           #+#    #+#             */
/*   Updated: 2019/05/20 22:28:36 by anhuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <ctype.h>

int main(int ac, char *v[])
{
	if (ac <= 1)
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
		return 0;
	}
	else if (ac > 1)
	{
		for (int y = 1; y < ac; y++)
		{
			for (int x = 0; v[y][x] != '\0'; x++)
			{
				v[y][x] = toupper(v[y][x]);
				std::cout << v[y][x];
			}
		}
		std::cout << std::endl;
	}
	return 0;
}
