/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anhuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 22:28:25 by anhuang           #+#    #+#             */
/*   Updated: 2019/05/20 22:28:27 by anhuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
#define CONTACT_HPP

#include <string>
#include <iostream>
#include <iomanip>

class Contact
{
	public:
		Contact(void);
		~Contact(void);

		void add(void);
		std::string checkAdd(std::string);
		void clearbuf(void);

		std::string get_fname(void) const ;
		std::string get_lname(void) const ;
		std::string get_nickname(void) const ;
		std::string get_login(void) const ;
		std::string get_postal(void) const ;
		std::string get_email(void) const ;
		std::string get_pnumber(void) const ;
		std::string get_birthday(void) const ;
		std::string get_fmeal(void) const ;
		std::string get_ucolor(void) const ;
		std::string get_dsecret(void) const ;

		void set_fname(std::string str);
		void set_lname(std::string str);
		void set_nickname(std::string str);
		void set_login(std::string str);
		void set_postal(std::string str);
		void set_email(std::string str);
		void set_pnumber(std::string str);
		void set_birthday(std::string str);
		void set_fmeal(std::string str);
		void set_ucolor(std::string str);
		void set_dsecret(std::string str);

	private:
		std::string _fname;
		std::string _lname;
		std::string _nickname;
		std::string _login;
		std::string _postal;
		std::string _email;
		std::string _pnumber;
		std::string _birthday;
		std::string _fmeal;
		std::string _ucolor;
		std::string _dsecret;

		static int	_nbContact;
};

#endif
