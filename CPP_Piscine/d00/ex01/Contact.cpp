/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anhuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 22:28:03 by anhuang           #+#    #+#             */
/*   Updated: 2019/05/20 22:28:04 by anhuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"

Contact::Contact(void)
{
	if (Contact::_nbContact == 8) {
		std::cout << "Maximum Contact Reached." << std::endl;
		return ;
	}
	Contact::_nbContact++;
}

Contact::~Contact(void)
{
}

void Contact::add(void)
{
	std::cout << "#### Creating Contact ####" << std::endl;

	this->_fname = checkAdd("First Name: ");
	this->_lname = checkAdd("Last Name: ");
	this->_nickname = checkAdd("Nickname: ");
	this->_login = checkAdd("Login: ");
	this->_postal = checkAdd("Postal Address: ");
	this->_email = checkAdd("Email Address: ");
	this->_pnumber = checkAdd("PhoneNumber: ");
	this->_birthday = checkAdd("Birthday Date: ");
	this->_fmeal = checkAdd("Favorite Meal: ");
	this->_ucolor = checkAdd("UnderWear Color: ");
	this->_dsecret = checkAdd("Darkest Secret: ");

	std::cout << "#### Contact Added ####" << std::endl;
}

std::string Contact::checkAdd(std::string msg)
{
	std::string buf;

	std::cout << msg;
	std::getline(std::cin, buf);

	return buf;
}

std::string Contact::get_fname(void) const { return this->_fname; }
std::string Contact::get_lname(void) const { return this->_lname; }
std::string Contact::get_nickname(void) const { return this->_nickname; }
std::string Contact::get_login(void) const { return this->_login; }
std::string Contact::get_postal(void) const { return this->_postal; }
std::string Contact::get_email(void) const { return this->_email; }
std::string Contact::get_pnumber(void) const { return this->_pnumber; }
std::string Contact::get_birthday(void) const { return this->_birthday; }
std::string Contact::get_fmeal(void) const { return this->_fmeal; }
std::string Contact::get_ucolor(void) const { return this->_ucolor; }
std::string Contact::get_dsecret(void) const { return this->_dsecret; }

int Contact::_nbContact = 0;
