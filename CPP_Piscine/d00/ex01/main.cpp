/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anhuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 22:28:17 by anhuang           #+#    #+#             */
/*   Updated: 2019/05/20 22:28:20 by anhuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.hpp"

std::string resizeWide(std::string str)
{
	if (str.length() > 10)
		return str.replace(9, 1, ".").erase(10);
	return str;
}

void print_info(Contact ct)
{
	std::cout << "###   INFO   ###" << std::endl \
		<< "First Name : " << ct.get_fname() << std::endl \
		<< "Last Name : " << ct.get_lname() << std::endl \
		<< "Nickname : " << ct.get_nickname() << std::endl \
		<< "Login : " << ct.get_login() << std::endl \
		<< "Postal Address : " << ct.get_postal() << std::endl \
		<< "Email Address : " << ct.get_email() << std::endl \
		<< "PhoneNumber : " << ct.get_pnumber() << std::endl \
		<< "Birthday Date : " << ct.get_birthday() << std::endl \
		<< "Favorite Meal : " << ct.get_fmeal() << std::endl \
		<< "UnderWear Color : " << ct.get_ucolor() << std::endl \
		<< "Darkest Secret : " << ct.get_dsecret() << std::endl \
		<< "### END INFO ###" << std::endl;
}

bool isNumber(std::string s)
{
	for (unsigned long i = 0; i < s.length(); i++)
		if (std::isdigit(s[i]) == false)
			return false;
	return true;
}

void search(Contact *contact, int index)
{
	std::string index_info;

	std::cout << std::right << std::setw(10) << "index" << "|" \
				<< std::right << std::setw(10) << "first name" << "|" \
				<< std::right << std::setw(10) << "last name" << "|" \
				<< std::right << std::setw(10)<< "nickname" << std::endl;

	for (int i = 0; i < index; i++) {
		std::cout << std::right << std::setw(10) << (i + 1) << "|" \
			<< std::right << std::setw(10) << resizeWide(contact[i].get_fname()) << "|" \
			<< std::right << std::setw(10) << resizeWide(contact[i].get_lname()) << "|" \
			<< std::right << std::setw(10) << resizeWide(contact[i].get_nickname()) << std::endl;
	}
	while (true)
	{
		std::cout << "Choose Index : [1 - " << index << "]" << std::endl;

		std::getline(std::cin, index_info);
		if (index_info.empty() || !isNumber(index_info) || std::stoi(index_info) > index || std::stoi(index_info) <= 0)
			std::cout << "Error Choose Index 1 - " << index << "]" << std::endl;
		else
			break;
	}
	print_info(contact[std::stoi(index_info) - 1]);
}

int main()
{
	std::string buf;
	Contact contact[8];
	int index = 0;
	std::cout << "Welcome on your PhoneBook !" << std::endl << "Say one of these command : ADD / SEARCH / EXIT" << std::endl;

	while (std::getline(std::cin, buf))
	{
		if (!buf.compare("ADD"))
		{
			if (index >= 8)
			{
				std::cout << "Maximum contact reached" << std::endl;
				continue;
			}
			contact[index].add();
			index++;
			std::cout << "Say one of these command : ADD / SEARCH / EXIT" << std::endl;
		}
		else if (!buf.compare("SEARCH"))
		{
			if (index == 0) {
				std::cout << "No contact Added." << std::endl \
				<< "Say one of these command : ADD / SEARCH / EXIT" << std::endl;
				continue;
			}
			search(contact, index);
			std::cout << "Say one of these command : ADD / SEARCH / EXIT" << std::endl;
		}
		else if (!buf.compare("EXIT"))
			break;
		else
			std::cout << "Wrong command, please use : ADD / SEARCH / EXIT" << std::endl;
	}
	return 0;
}
