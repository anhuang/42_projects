/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anhuang <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 22:27:51 by anhuang           #+#    #+#             */
/*   Updated: 2019/05/20 22:27:53 by anhuang          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Account.class.hpp"
#include <iostream>
#include <time.h>

Account::Account(int initial_deposit) : _accountIndex(Account::getNbAccounts()), _amount(initial_deposit), \
										_nbDeposits(0), _nbWithdrawals(0)
{
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex << \
	";amount:" << initial_deposit << \
	";created" << std::endl;

	Account::_totalAmount += initial_deposit;
	Account::_nbAccounts++;
}

Account::~Account(void)
{
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex << \
		";amount:" << this->_amount << \
		";closed" << std::endl;

	Account::_nbAccounts--;
}

void Account::makeDeposit(int deposit)
{
	this->_nbDeposits++;
	Account::_totalNbDeposits++;
	Account::_totalAmount += deposit;

	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex << \
	";p_amount:" << this->_amount << \
	";deposit:" << deposit << \
	";amount:" << this->_amount + deposit <<
	";nb_deposits:" << this->_nbDeposits << std::endl;

	this->_amount += deposit;
}

bool Account::makeWithdrawal(int withdrawal)
{
	if (this->_amount < withdrawal) {
		Account::_displayTimestamp();
		std::cout << " index:" << this->_accountIndex << \
			";p_amount:" << this->_amount << \
			";withdrawal:refused" << std::endl;
			return 1;
	}
	else {
		Account::_totalAmount -= withdrawal;
		this->_nbWithdrawals++;
		Account::_totalNbWithdrawals++;

		Account::_displayTimestamp();
		std::cout << " index:" << this->_accountIndex << \
			";p_amount:" << this->_amount << \
			";withdrawal:" << withdrawal << \
			";amount:" << this->_amount - withdrawal << \
			";nb_withdrawals:" << this->_nbWithdrawals << std::endl;
	}
	this->_amount -= withdrawal;
	return 0;
}

int Account::checkAmount(void) const
{
	_nbTimesPassed++;
	return 0;
}

void Account::displayStatus(void) const
{
	Account::_displayTimestamp();
	std::cout << " index:" << this->_accountIndex << \
	";amount:" << this->_amount << \
	";deposits:" << this->_nbDeposits << \
	";withdrawals:" << this->_nbWithdrawals << std::endl;
}

void Account::displayAccountsInfos(void)
{
	Account::_displayTimestamp();
	std::cout << " accounts:" << Account::getNbAccounts() \
		<< ";total:" << Account::getTotalAmount() \
		<< ";deposits:" << Account::getNbDeposits() \
		<< ";withdrawals:" << Account::getNbWithdrawals() << std::endl;
}

void Account::_displayTimestamp(void)
{
	std::time_t rawtime;
	struct tm * timeinfo;
	char buffer [80];

	std::time(&rawtime);
	timeinfo = std::localtime(&rawtime);
	std::strftime (buffer, 80, "[%Y%m%d_%H%M%S]",timeinfo);
	std::cout << buffer;
}

int Account::getNbAccounts(void) { return Account::_nbAccounts; }
int Account::getTotalAmount(void) { return Account::_totalAmount; }
int Account::getNbDeposits(void) { return Account::_totalNbDeposits; }
int Account::getNbWithdrawals(void) { return Account::_totalNbWithdrawals; }

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;
