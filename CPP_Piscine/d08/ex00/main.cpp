#include "easyfind.hpp"

int main(void)
{
	int myints[] = { 10, 20, 30, 40 };
	std::vector<int> fifth (myints, myints + sizeof(myints) / sizeof(int) );
	std::cout << *(easyfind< std::vector<int> >(fifth, 30)) << std::endl;
	return (0);
}
