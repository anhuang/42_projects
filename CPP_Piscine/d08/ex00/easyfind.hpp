#include <iostream>
#include <algorithm>
#include <vector>

class Error : public std::exception
{
	public :
		const char* what() const throw() {
			return "Exception ! Not Found :(";
		}
};

template<typename T>
typename T::iterator easyfind(T& array, int i) {
	typename T::iterator it = find(array.begin(), array.end(), i);
	if (it != array.end())
		return it;
	throw Error();
}
