#ifndef MUSTANTSTACK_HPP
# define MUSTANTSTACK_HPP

#include <iostream>
#include <deque>
#include <stack>
#include <list>

template< typename T, typename Container = std::deque<T> >
class MutantStack : public std::stack< T, Container >
{
	private:
	protected:

	public:
		MutantStack(void);
		MutantStack< T, Container >(MutantStack< T, Container > const &src);
		virtual ~MutantStack< T, Container >(void);

		MutantStack< T, Container > &	operator=(MutantStack< T, Container > const &rhs);

		typename Container::iterator begin();
		typename Container::iterator end();

		typename Container::const_iterator begin() const;
		typename Container::const_iterator end() const;

		typedef typename Container::iterator iterator;
		typedef typename Container::const_iterator const_iterator;
};

#endif
