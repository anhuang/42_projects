#include "mutantstack.hpp"
#include "mutantstack.cpp"

int						main(void)
{
	MutantStack<int>	stack;

	stack.push(1);
	stack.push(3);

	std::cout << "Top : " << stack.top() << std::endl;
	std::cout << "Size : " << stack.size() << " Then pop.. " << std::endl;

	stack.pop();

	std::cout << "Size : " << stack.size() << std::endl;

	stack.push(5);
	stack.push(12);
	stack.push(737);
	stack.push(0);

	MutantStack<int>::iterator	it = stack.begin();
	MutantStack<int>::iterator	ite = stack.end();

	std::cout << "Mutant stack content: " << std::endl;
	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}

	std::cout << "List content must be same: " << std::endl;
	std::list<int>				lst;

	lst.push_back(1);
	lst.push_back(3);
	lst.pop_back();
	lst.push_back(5);
	lst.push_back(12);
	lst.push_back(737);
	lst.push_back(0);

	MutantStack<int>::iterator	it2 = stack.begin();
	MutantStack<int>::iterator	ite2 = stack.end();

	while (it2 != ite2)
	{
		std::cout << *it2 << std::endl;
		++it2;
	}

	return (0);
}
