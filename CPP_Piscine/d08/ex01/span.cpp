#include "span.hpp"

Span::Span(unsigned int nb) : _vInt(nb), _index(0){
}

Span::Span(void) {
}

Span::Span(Span const &src) {
	*this = src;
}

Span::~Span(void) {
}

Span &	Span::operator=(Span const &rhs) {
	if (this != &rhs) {
		_vInt = rhs.getvInt();
		_index = rhs.getIndex();
		rhs.getvInt().clear();
	}
	return *this;
}

void Span::addNumber(int nb) {
	if (_index >= _vInt.size())
		throw LimitError();
	_vInt[0] = nb;
	sort(_vInt.begin(), _vInt.end());
	_index++;
}

void Span::addRangeNumbers(std::vector<int>::iterator it1, std::vector<int>::iterator it2) {
	if (static_cast<long>(_vInt.size() - _index) < it2 - it1)
		throw Range();
	while (it1 != it2) {
		_vInt[_index] = *it1;
		_index++;
		it1++;
	}
	sort(_vInt.begin(), _vInt.end());
}

unsigned int Span::shortestSpan() {
	if (_index < 2)
		throw Error();
	int ret = INT_MAX;
	std::vector<int>::iterator it;
	for (it = _vInt.begin(); it != _vInt.end() - 1; it++) {
		if ((*(it + 1) - *it) < ret)
			ret = (*(it + 1) - *it);
	}
	return ret;
}

unsigned int Span::longestSpan() {
	if (_index < 2)
		throw Error();
	return _vInt.back() - _vInt.front();
}

const char* Span::Error::what() const throw() {
	return "Exception ! There’s no numbers stored, or only one, there is no span to find.";
}

const char* Span::LimitError::what() const throw() {
	return "Exception ! Max numbers Added Reached !";
}

const char* Span::Range::what() const throw() {
	return "Exception ! Range too big !";
}

std::vector<int> Span::getvInt() const { return _vInt; }
unsigned int Span::getIndex() const { return _index; }
