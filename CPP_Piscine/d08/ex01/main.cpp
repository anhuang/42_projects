#include "span.hpp"

void testSpan(Span &sp, bool shorted) {
	try {
		if (shorted)
			std::cout << "shortestSpan: " << sp.shortestSpan() << std::endl;
		else
			std::cout << "longestSpan: " << sp.longestSpan() << std::endl;
	}
	catch(const std::exception & e) {
		std::cerr << e.what() << std::endl;
	}
}

int main()
{
	Span sp = Span(10000);
	srand(time(0));

	try {
		for (size_t i = 0; i < 10000; i++) {
			sp.addNumber(std::rand());
		}
		std::cout << "Short : " << sp.shortestSpan() << std::endl;
		std::cout << "Long : " << sp.longestSpan() << std::endl;
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	struct IncGenerator {
	    int current_;
	    IncGenerator (int start) : current_(start) {}
	    int operator() () { return current_++; }
	};

	std::vector<int> v(100) ;
	IncGenerator g (0);
	std::generate( v.begin(), v.end(), std::rand);

	Span sp2 = Span(100);
	try {
		sp2.addRangeNumbers(v.begin(), v.end());
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
	testSpan(sp2, true);
	testSpan(sp2, false);
}
