#ifndef SPAN_HPP
# define SPAN_HPP

#include <iostream>
#include <string>
#include <vector>
#include <climits>

class Span
{
	private:
		std::vector<int> _vInt;
		unsigned int _index;
		Span(void);
	protected:

	public:
		Span(unsigned int n);
		Span(Span const &src);
		Span &	operator=(Span const &rhs);
		virtual ~Span(void);

		void addNumber(int nb);
		void addRangeNumbers(std::vector<int>::iterator it1, std::vector<int>::iterator it2);

		unsigned int shortestSpan();
		unsigned int longestSpan();

		std::vector<int> getvInt() const;
		unsigned int getIndex() const;
	class Error : public std::exception {
		public :
			const char* what() const throw();
	};
	class LimitError : public std::exception {
		public :
			const char* what() const throw();
	};
	class Range : public std::exception {
		public :
			const char* what() const throw();
	};
};

//std::ostream &	operator<<(std::ostream &o, Span const &rhs);

#endif
