#include <iostream>
#include <string>
#include <stdio.h>

template< typename T>
void display(T const & msg) {
	std::cout << msg;
}

void isOdd(int const & nb) {
	if (nb % 2 == 0)
		std::cout << nb << " is Even." << std::endl;
	else
		std::cout << nb << " is Odd." << std::endl;
}

void uppercase(char const & c) {
	char temp = toupper(c);
	std::cout << temp;
}

template< typename T, typename L>
void iter(T * array, L const & len, void (&f)(T const &)) {
	for (L i = 0; i < len; i++) {
		f(array[i]);
	}
}

int main(void)
{
	char arrayT[] = "Bonjour Tout le monde";
	int arrayInt[] = {1, 23, 56, 91};

	std::cout << "BEFORE: " << arrayT << std::endl;
	std::cout << "AFTER : ";
	iter(arrayT, std::strlen(arrayT), uppercase);
	std::cout << std::endl;
	iter(arrayInt, 4, isOdd);
	iter(arrayT, std::strlen(arrayT), display);
	std::cout << std::endl;
	return (0);
}
