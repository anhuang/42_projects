#include "Array.cpp"

int main(void) {

	srand(time(0));
	int	n = rand() % 10;
	Array<int> test(n);
	std::cout << "Size of Array made: " << test.size() << std::endl << std::endl;

	try
	{
		unsigned int i = 0;
		while (i < test.size())
		{
			test[i] = rand() % 100;
			i++;
		}
		i = 0;
		while (i <= test.size())
		{
			std::cout << "Array[" << i << "] Content: " << test[i] << std::endl;
			i++;
		}
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}

	Array <int> a(10);
	std::cout << "Array of ints:	";
	try {
		for (int i = 0; i < 10; i++) {
			a[i] = i + 1;
			std::cout << a[i] << " ";
		}
		std::cout << std::endl;
		std::cout	<< "Trying to display value in array that index 10:	"
					<< a[10] << std::endl;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	Array <std::string> b(3);
	try {
		b[0] = "Hello";
		b[1] = "World";
		b[2] = "!";
		std::cout << "Array of std::strings:				";
		for(int i = 0; i < 3; i++)
			std::cout << b[i] << " ";
		std::cout << std::endl;
		std::cout << "Trying to reach element with index 1:		" << b[1] << std::endl;
		std::cout << "Element with index 4:				" << b[4] << std::endl;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	Array<double> c(5);
	try {
		std::cout << "Array of floats:				";
		for(int i = 0; i < 5; i++) {
			c[i] = (i * 0.1 + 1) * 0.078;
			std::cout << c[i] << " ";
		}
		std::cout << std::endl;
		std::cout << "Element number 1:				" << c[1] << std::endl;
		std::cout << "Element number 10:				" << c[10] << std::endl;
	}
	catch(std::exception & e) {
		std::cout << e.what() << std::endl;
	}
	Array<int> nullArray;
	try{
		std::cout << "nullArray: " << nullArray[0] << std::endl;
	}
	catch(const std::exception& e) {
		std::cerr << e.what() << '\n';
	}

	Array<int> test3(5);
	std::cout << test3[0] << std::endl;
	for (unsigned int i = 0; i < test3.size(); i++)
		test3[i] = rand();
	for (unsigned int i = 0; i < test3.size(); i++)
		std::cout << test3[i] << std::endl;

	std::cout << std::endl;
	Array<int> test2 = test3;
	for (unsigned int i = 0; i < test2.size(); i++)
		std::cout << test2[i] << std::endl;
}
