#ifndef ARRAY_TPP
# define ARRAY_TPP

#include <iostream>
#include <string>

template<typename T>
class Array
{
	private:
		T * _data;
		unsigned int _size;
	protected:

	public:
		Array(void) : _data(new T()), _size(1) {
		}
		Array<T>(unsigned int n) : _data(new T[n]), _size(n) {
		}
		Array<T>(Array<T> const &src) {
			*this = src;
		}
		virtual ~Array<T>(void) {
			delete [] _data;
		}

		T &	operator[](unsigned int n) const {
			if (n >= size() || n < 0)
				throw Array::OutOfLimits();
			return _data[n];
		}

		Array &	operator=(Array const &rhs) {
			delete [] _data;
			if (rhs.size() > 0)
				_data = new T[rhs.size()];
			else
				_data = new T();
			for (unsigned int i = 0; i < rhs.size(); i++)
				_data[i] = rhs[i];
			_size = rhs.size();
			return *this;
		}

		class OutOfLimits : public std::exception
		{
			public :
				const char* what() const throw() {
					return "STOP You Should not PASSSSSSSSSSS ! OutOfLimits !";
				}
		};

		unsigned int size() const { return _size; }
};

//std::ostream &	operator<<(std::ostream &o, Array const &rhs);
#endif
