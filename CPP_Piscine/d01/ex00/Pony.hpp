#ifndef PONY_HPP
#define PONY_HPP

#include <string>
#include <iostream>

class Pony
{
	public :
		Pony(std::string name, std::string age, std::string speed);
		~Pony();

		void setSpeed(std::string);
		void setAge(std::string);
		void setName(std::string);

		std::string getName();
		std::string getAge();
		std::string getSpeed();

	private :
		std::string _name;
		std::string _age;
		std::string _speed;
};

#endif
