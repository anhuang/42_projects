#include "Pony.hpp"
#include <iostream>

void ponyOnTheHeap()
{
	Pony *pn = new Pony("Isabelle", "2", "210");
	std::cout << "Hello my name is " << pn->getName() << ", I " << pn->getAge()\
	<< " years old and I'm galloping at " << pn->getSpeed() << "mph." << std::endl;

	delete pn;
}

void ponyOnTheStack()
{
	Pony pn = Pony("Ella", "3", "170");
	std::cout << "Hello my name is " << pn.getName() << ", I " << pn.getAge()\
	<< " years old and I'm galloping at " << pn.getSpeed() << "mph." << std::endl;
}

int main() {
	std::cout << "Pony On The Heap :" << std::endl;
	ponyOnTheHeap();

	std::cout << "Pony On The Stack :" << std::endl;
	ponyOnTheStack();
	return 0;
}
