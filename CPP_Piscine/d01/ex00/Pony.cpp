#include "Pony.hpp"

Pony::Pony(std::string name, std::string age, std::string speed) : _name(name),
													_age(age), _speed(speed)
{
	std::cout << "Pony Constructed" << std::endl;
}

Pony::~Pony(){
	std::cout << "Pony Destroyed" << std::endl;
}

void Pony::setName(std::string str) { this->_name = str; }
void Pony::setAge(std::string str) { this->_age = str; }
void Pony::setSpeed(std::string str) { this->_speed = str; }

std::string Pony::getName() { return this->_name; }
std::string Pony::getAge() { return this->_age; }
std::string Pony::getSpeed() { return this->_speed; }
