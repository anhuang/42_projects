#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n) : _sizeHorde(n)
{
	std::cout << "- Zombiehorde Constructor -" << std::endl;
	srand(time(NULL));

	this->_hordeOfZombie = new Zombie[n];
	std::string names[] = {"Ella", "Isabelle", "Daniel", "Timothee"};

	for (int i = 0; i < n; i++) {
		this->_hordeOfZombie[i].setName(names[std::rand() % (sizeof(names)/sizeof(names[0]))]);
	}
	ZombieHorde::announce();
}

ZombieHorde::~ZombieHorde()
{
	std::cout << "- ZombieHorde Destructor -" << std::endl;
	delete [] _hordeOfZombie;
}

void ZombieHorde::announce() const
{
	for (int i = 0; i < this->_sizeHorde; i++) {
		_hordeOfZombie[i].announce();
	}
}
