#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) : _name(name), _type(type)
{
}

Zombie::Zombie() : _name("Default_Name"), _type("Default")
{
	std::cout << "- Zombie Constructor -" << std::endl;
}

Zombie::~Zombie()
{
	std::cout << "- Zombie Destructor -" << std::endl;
}

void Zombie::announce() const
{
	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss..." << std::endl; // a choisir un des deux
}

void Zombie::setType(std::string str) { this->_type = str; }
void Zombie::setName(std::string str) { this->_name = str; }

std::string Zombie::getName() const { return this->_name; }
