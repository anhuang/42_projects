#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <stdio.h>
#include <string>
#include <iostream>
#include <stdlib.h>

class Zombie{
	public :
		Zombie(std::string, std::string);
		Zombie();
		~Zombie();

		void announce() const;

		void setType(std::string);

	private :
		std::string _name;
		std::string _type;
};

#endif
