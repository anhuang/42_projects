#ifndef ZOMBIEEVENT_HPP
#define ZOMBIEEVENT_HPP

#include "Zombie.hpp"

class ZombieEvent{
	public :
		ZombieEvent();
		~ZombieEvent();

		void setZombieType(std::string);
		Zombie* newZombie(std::string);
		void randomChump();

		std::string getType() const;

	private :
		std::string _typeZombie;
};

#endif
