#include "ZombieEvent.hpp"

int main(void)
{
	ZombieEvent zEvent = ZombieEvent();
	Zombie* zombieNormal = new Zombie();

	zombieNormal->announce();
	zEvent.setZombieType("Carnivore");
	Zombie* zb = zEvent.newZombie("You");
	zEvent.randomChump();
	zb->announce();

	delete zb;
	delete zombieNormal;
	return (0);
}
