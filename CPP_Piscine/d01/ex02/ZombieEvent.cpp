#include "ZombieEvent.hpp"
#include <ctime>

ZombieEvent::ZombieEvent() : _typeZombie("Default")
{

}

ZombieEvent::~ZombieEvent()
{

}


void ZombieEvent::setZombieType(std::string type) {
	_typeZombie = type;
	std::cout << "[UPDATE] All new zombies will be of type : " << _typeZombie << std::endl;
}

Zombie* ZombieEvent::newZombie(std::string name) {
	return (new Zombie(name, this->_typeZombie));
}

void ZombieEvent::randomChump()
{
	srand(time(NULL));
	std::string names[4] = {"Ella", "Isabelle", "Daniel", "Timothee"};
	Zombie zb = Zombie(names[std::rand() % (sizeof(names)/sizeof(names[0]))], this->_typeZombie);

	zb.announce();
}

std::string ZombieEvent::getType() const { return this->_typeZombie; }
