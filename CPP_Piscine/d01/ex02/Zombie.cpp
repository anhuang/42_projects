#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type) : _name(name), _type(type)
{
}

Zombie::Zombie() : _name("Default_Name"), _type("Default")
{
}

Zombie::~Zombie()
{

}

void Zombie::announce() const
{
	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss..." << std::endl; // a choisir un des deux
}

void Zombie::setType(std::string str) { this->_type = str; }
