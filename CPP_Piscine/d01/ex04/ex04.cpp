#include <iostream>

int main(void)
{
	std::string str = "HI THIS IS BRAIN";
	std::string* ptr = &str;
	std::string& ref = str;

	std::cout << "---POINTER---" << std::endl << \
			*ptr << std::endl << "---REFERENCE---" << std::endl << \
			ref << std::endl;
	return (0);
}
