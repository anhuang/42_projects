#ifndef LOGGER_HPP
# define LOGGER_HPP

#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include <sstream>

class Logger
{
	public:
		Logger(std::string);
		~Logger();
		void log(std::string const &, std::string const &);
	private:
		std::string makeLogEntry(std::string const &);
		void logToConsole(std::string const &);
		void logToFile(std::string const &);
		std::string _filename;
};

#endif
