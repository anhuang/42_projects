#include "Logger.hpp"

typedef void (Logger::*tabPtr)(std::string const & msg);

Logger::Logger(std::string name) : _filename(name)
{

}

Logger::~Logger()
{

}

void Logger::logToFile(std::string const & msg)
{
	std::ofstream ofs;
	ofs.open("Logger", std::ofstream::out | std::ofstream::app);
	ofs << msg << std::endl;
	ofs.close();
}

void Logger::logToConsole(std::string const & msg)
{
	std::cout << msg << std::endl;
}

std::string Logger::makeLogEntry(std::string const & msg)
{
	time_t     now = time(0);
	struct tm  tstruct;
	char	   buf[80];
	tstruct = *localtime(&now);

	strftime(buf, sizeof(buf), "[%Y-%m-%d.%X] ", &tstruct);

	return buf + msg;
}

struct arrayFun{
	tabPtr fn;
	const std::string key;
};

void Logger::log(std::string const & dest, std::string const & message)
{
	arrayFun tableOfFunction[] =
		{
			{ &Logger::logToFile, "logToFile"},
			{ &Logger::logToConsole, "logToConsole"},
		};

	int len = (sizeof(tableOfFunction)/sizeof(*tableOfFunction));
	for (int i = 0; i < len; i++) {
		if (!dest.compare(tableOfFunction[i].key))
		{
			(this->*tableOfFunction[i].fn)(makeLogEntry(message));
			return;
		}
	}
	std::cerr << "Error! dest not recognized" << std::endl;
}
