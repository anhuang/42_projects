#include "Human.hpp"


Human::Human(void) : _brain(new Brain())
{
}

Human::~Human(void)
{
	delete this->_brain;
}

std::string Human::identify() const
{
	return this->_brain->identify();
}

Brain const & Human::getBrain() const
{
	return *(this->_brain);
}
