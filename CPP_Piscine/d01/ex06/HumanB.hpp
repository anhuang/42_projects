#ifndef HUMANB_HPP
# define HUMANB_HPP

#include "Weapon.hpp"

class HumanB
{
	public:
		HumanB(std::string, Weapon*);
		HumanB(std::string);
		~HumanB();

		void setWeapon(Weapon &);
		void attack();
	private:
		std::string _name;
		Weapon * _wp;
};


#endif
