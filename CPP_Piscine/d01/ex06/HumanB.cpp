#include "HumanB.hpp"

HumanB::HumanB(std::string name, Weapon *wp) : _name(name), _wp(wp)
{
//	std::cout << " HumanA Default Constructor " << std::endl;
	return;
}

HumanB::HumanB(std::string name) : _name(name)
{
//	std::cout << " HumanA Default Constructor " << std::endl;
	return;
}

HumanB::~HumanB(void)
{
//	std::cout << " HumanB Destructor " << std::endl;
	return;
}

void HumanB::attack()
{
	std::cout << this->_name << " attacks with his " << this->_wp->getType() << std::endl;
}

void HumanB::setWeapon(Weapon &wp)
{
	_wp = &wp;
}
