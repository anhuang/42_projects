#include "Weapon.hpp"


Weapon::Weapon(std::string type) : _type(type)
{
	// std::cout << " Weapon Default Constructor " << std::endl;
	return;
}

Weapon::~Weapon(void)
{
	// std::cout << " Weapon Destructor " << std::endl;
	return;
}

std::string const & Weapon::getType() const { return this->_type; }
void Weapon::setType(std::string type) { this->_type = type; }
