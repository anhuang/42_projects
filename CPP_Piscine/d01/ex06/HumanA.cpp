#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon &wp) : _name(name), _wp(wp)
{
}

HumanA::~HumanA(void)
{
//	std::cout << " HumanA Destructor " << std::endl;
	return;
}

void HumanA::attack()
{
	std::cout << this->_name << " attacks with his " << this->_wp.getType() << std::endl;
}
//
// void HumanA::setWeapon(Weapon &wp)
// {
// 	_wp = &wp;
// }
