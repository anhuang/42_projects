#ifndef _CLASS_HPP
# define _CLASS_HPP

#include "Weapon.hpp"

class HumanA
{
	public:
		HumanA(std::string, Weapon&);
		~HumanA();

		void setWeapon(Weapon&);
		void attack();
	private:
		std::string _name;
		Weapon & _wp;
};

#endif
