#include "Human.hpp"

typedef void (Human::*tabPtr)(std::string const & target);

void Human::meleeAttack(std::string const & target)
{
	std::cout << "MeleeAttack on " << target << std::endl;
}

void Human::rangedAttack(std::string const & target)
{
	std::cout  << "RangedAttack on " << target << std::endl;
}

void Human::intimidatingShout(std::string const & target)
{
	std::cout << "IntimidatingShout on"  << target << std::endl;
}

struct arrayFun{
	tabPtr fn;
	const std::string key;
};

void Human::action(std::string const & action_name, std::string const & target)
{
	arrayFun tableOfFunction[] =
		{
			{ &Human::meleeAttack, "meleeAttack"},
			{ &Human::rangedAttack, "rangedAttack"},
			{ &Human::intimidatingShout, "intimidatingShout"}
		};

	int len = (sizeof(tableOfFunction)/sizeof(*tableOfFunction));
	for (int i = 0; i < len; i++) {
		if (!action_name.compare(tableOfFunction[i].key))
		{
			(this->*tableOfFunction[i].fn)(target);
			return;
		}
	}
	std::cerr << "Error! action_name not recognized" << std::endl;
}
