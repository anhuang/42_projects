#include <iostream>
#include <fstream>
#include <sstream>

void findAndReplaceAll(std::string & data, std::string toSearch, std::string replaceStr)
{
	// Get the first occurrence
	size_t pos = data.find(toSearch);

	// Repeat till end is reached
	while(pos != std::string::npos)
	{
		// Replace this occurrence of Sub String
		data.replace(pos, toSearch.size(), replaceStr);
		// Get the next occurrence from the current position
		pos =data.find(toSearch, pos + replaceStr.size());
	}
}

int main(int ac, char*av[])
{
	if (ac != 4){
		std::cout << "Error Parameters, need 3 Parameters : [File] [String Search] [String Replace]" << std::endl;
		return 1;
	}

	std::string test = av[1];
	std::ifstream ifs(test);
	std::ofstream ofs(test + ".replace");
	if (!ifs || !ofs) {
		std::cerr << "Your File in parameters is not valid." << std::endl;
		return 1;
	}
	std::string s1(av[2]);
	std::string s2(av[3]);

	std::ostringstream ss;
	ss << ifs.rdbuf();
	std::string res = ss.str();

	// std::ofstream ofs("FILENAME.replace"); // Create file
	findAndReplaceAll(res, s1, s2); // Change buffer res
	ofs << res; // Write Buffer in the new file

	//TEST
	std::cout << "===OLD===" << ss.str() << std::endl;
	std::cout << "===NEW===" << res << std::endl;
	ifs.close();

	return (0);
}
