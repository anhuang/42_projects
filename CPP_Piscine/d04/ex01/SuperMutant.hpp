#ifndef SUPERMUTANT_CLASS_HPP
# define SUPERMUTANT_CLASS_HPP

#include <iostream>
#include <string>
#include "Enemy.hpp"

class SuperMutant : public Enemy
{
	private:

	protected:


	public:
		SuperMutant(void);
		SuperMutant(SuperMutant const &src);
		~SuperMutant(void);

		virtual void takeDamage(int);

		SuperMutant &	operator=(SuperMutant const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, SuperMutant const &rhs);

#endif
