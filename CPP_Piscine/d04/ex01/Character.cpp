#include "Character.hpp"


Character::Character(void)
{
//	std::cout << " Character Default Constructor " << std::endl;
	return;
}

Character::Character(std::string const & name) : _name(name), _actionPoints(40), _aWeapon(NULL)
{
	return ;
}


Character::Character(Character const &src)
{
//	std::cout << " Character Copy Constructor " << std::endl;
	*this = src;
	return;
}

Character::~Character(void)
{
//	std::cout << " Character Destructor " << std::endl;
	return;
}

Character &	Character::operator=(Character const &rhs)
{
//	std::cout << " Character Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_actionPoints = rhs.getAP();
		this->_aWeapon = rhs.getWP();
	}
	return *this;
}

void Character::recoverAP() {
	_actionPoints += 10;
	_actionPoints = _actionPoints > 40 ? 40 : _actionPoints;
}
void Character::equip(AWeapon* newWp) {
	_aWeapon = newWp;
}
void Character::attack(Enemy* enemy) {
	if (_actionPoints < _aWeapon->getAPCost()) {
		std::cout << "No AP, No Attack." << std::endl;
		return;
	}
	std::cout << _name << " attacks " << enemy->getType() << " with a "<< _aWeapon->getName() << std::endl;
	_aWeapon->attack();
	_actionPoints -= _aWeapon->getAPCost();
	enemy->takeDamage(_aWeapon->getDamage());
	if (enemy->getHP() <= 0)
		delete enemy;
}

std::ostream &	operator<<(std::ostream &o, Character const &rhs) {
	if (rhs.getWP())
		o << rhs.getName() << " has " << rhs.getAP() << " AP and wields a " << rhs.getWP()->getName() << std::endl;
	else
		o << rhs.getName() << " has " << rhs.getAP() << " AP and is unarmed" << std::endl;
	return o;
}

int Character::getAP() const { return _actionPoints; }
AWeapon* Character::getWP() const { return _aWeapon; }
std::string const& Character::getName() const { return _name; }
