#include "Enemy.hpp"

Enemy::Enemy(void)
{
//	std::cout << " Enemy Default Constructor " << std::endl;
	return;
}

Enemy::Enemy(int hp, std::string const & type) : _hp(hp), _type(type)
{
	return;
}


Enemy::Enemy(Enemy const &src)
{
//	std::cout << " Enemy Copy Constructor " << std::endl;
	*this = src;
	return;
}

Enemy::~Enemy(void)
{
//	std::cout << " Enemy Destructor " << std::endl;
	return;
}

Enemy &	Enemy::operator=(Enemy const &rhs)
{
//	std::cout << " Enemy Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_type = rhs.getType();
		this->_hp = rhs.getHP();
	}

	return *this;
}

void Enemy::takeDamage(int dmg) {
	_hp -= dmg;
	_hp = _hp < 0 ? 0 : _hp;
}

std::string const& Enemy::getType() const { return _type; }
int Enemy::getHP() const { return _hp; }
