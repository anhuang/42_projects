#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle(void) : AWeapon("Plasma Rifle", 5, 21)
{
//	std::cout << " PlasmaRifle Default Constructor " << std::endl;
	return;
}

PlasmaRifle::PlasmaRifle(PlasmaRifle const &src)
{
//	std::cout << " PlasmaRifle Copy Constructor " << std::endl;
	*this = src;
	return;
}

PlasmaRifle::~PlasmaRifle(void)
{
//	std::cout << " PlasmaRifle Destructor " << std::endl;
	return;
}

PlasmaRifle &	PlasmaRifle::operator=(PlasmaRifle const &rhs)
{
//	std::cout << " PlasmaRifle Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_actionPoints = rhs.getAPCost();
		this->_damagePoints = rhs.getDamage();
}

	return *this;
}

void PlasmaRifle::attack() const {
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
}
