#ifndef ENEMY_CLASS_HPP
# define ENEMY_CLASS_HPP

#include <iostream>
#include <string>

class Enemy
{
	private:

	protected:
		int _hp;
		std::string _type;

	public:
		Enemy(void);
		Enemy(int hp, std::string const & type);
		Enemy(Enemy const &src);
		virtual ~Enemy();

		virtual void takeDamage(int);

		std::string const& getType() const;
		int getHP() const;

		Enemy &	operator=(Enemy const &rhs);
};

//std::ostream &	operator<<(std::ostream &o, Enemy const &rhs);

#endif
