#include "SuperMutant.hpp"


SuperMutant::SuperMutant(void) : Enemy(170, "Super Mutant")
{
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
//	std::cout << " SuperMutant Default Constructor " << std::endl;
	return;
}

SuperMutant::SuperMutant(SuperMutant const &src)
{
//	std::cout << " SuperMutant Copy Constructor " << std::endl;
	*this = src;
	return;
}

SuperMutant::~SuperMutant(void)
{
	std::cout << "Aaargh ..." << std::endl;
//	std::cout << " SuperMutant Destructor " << std::endl;
	return;
}

SuperMutant &	SuperMutant::operator=(SuperMutant const &rhs)
{
//	std::cout << " SuperMutant Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_type = rhs.getType();
		this->_hp = rhs.getHP();
	}
	return *this;
}

void SuperMutant::takeDamage(int dmg) {
	if (dmg <= 3 || _hp <= 0)
		return;
	_hp -= (dmg - 3);

	if (_hp < 0)
		_hp = 0;
}
