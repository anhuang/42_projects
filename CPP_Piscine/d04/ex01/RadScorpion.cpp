#include "RadScorpion.hpp"


RadScorpion::RadScorpion(void) : Enemy(80, "RadScorpion")
{
	std::cout << "* click click click *" << std::endl;
//	std::cout << " RadScorpion Default Constructor " << std::endl;
	return;
}

RadScorpion::RadScorpion(RadScorpion const &src)
{
//	std::cout << " RadScorpion Copy Constructor " << std::endl;
	*this = src;
	return;
}

RadScorpion::~RadScorpion(void)
{
	std::cout << "* SPROTCH *" << std::endl;
//	std::cout << " RadScorpion Destructor " << std::endl;
	return;
}

RadScorpion &	RadScorpion::operator=(RadScorpion const &rhs)
{
//	std::cout << " RadScorpion Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_type = rhs.getType();
		this->_hp = rhs.getHP();
	}
	return *this;
}

void RadScorpion::takeDamage(int dmg) {
	if (_hp <= 0)
		return;
	_hp -= dmg;
	if (_hp < 0)
		_hp = 0;
}
