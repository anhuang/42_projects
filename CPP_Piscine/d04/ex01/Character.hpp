#ifndef CHARACTER_CLASS_HPP
# define CHARACTER_CLASS_HPP

#include <iostream>
#include <string>
#include "AWeapon.hpp"
#include "Enemy.hpp"

class Character
{
	private:
		std::string _name;
		int _actionPoints;
		AWeapon* _aWeapon;

	protected:
		Character(void);

	public:
		Character(std::string const & name);
		Character(Character const &src);
		~Character();

		Character &	operator=(Character const &rhs);
		void recoverAP();
		void equip(AWeapon*);
		void attack(Enemy*);
		std::string const& getName() const;
		int getAP() const;
		AWeapon* getWP() const;
};

std::ostream &	operator<<(std::ostream &o, Character const &rhs);

#endif
