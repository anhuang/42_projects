#ifndef PLASMARIFLE_CLASS_HPP
# define PLASMARIFLE_CLASS_HPP

#include "AWeapon.hpp"


class PlasmaRifle : public AWeapon
{
	private:

	protected:

	public:
		PlasmaRifle(void);
		PlasmaRifle(PlasmaRifle const &src);
		~PlasmaRifle(void);

		virtual void attack() const;

		PlasmaRifle &	operator=(PlasmaRifle const &rhs);
};

//std::ostream &	operator<<(std::ostream &o, PlasmaRifle const &rhs);

#endif
