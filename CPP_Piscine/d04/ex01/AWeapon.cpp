#include "AWeapon.hpp"


AWeapon::AWeapon(void)
{
//	std::cout << " AWeapon Default Constructor " << std::endl;
	return;
}

AWeapon::AWeapon(std::string const &name, int apost, int damage) : \
			_name(name), _damagePoints(damage), _actionPoints(apost)
{
	return ;
}

AWeapon::AWeapon(AWeapon const &src)
{
//	std::cout << " AWeapon Copy Constructor " << std::endl;
	*this = src;
	return;
}

AWeapon::~AWeapon(void)
{
//	std::cout << " AWeapon Destructor " << std::endl;
	return;
}

AWeapon &	AWeapon::operator=(AWeapon const &rhs)
{
//	std::cout << " AWeapon Assignement Operator " << std::endl;

	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_actionPoints = rhs.getAPCost();
		this->_damagePoints = rhs.getDamage();
	}

	return *this;
}

std::string const& AWeapon::getName() const { return _name; }
int AWeapon::getDamage() const { return _damagePoints; }
int AWeapon::getAPCost() const { return _actionPoints; }
