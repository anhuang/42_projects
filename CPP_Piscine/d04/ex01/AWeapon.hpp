#ifndef AWEAPON_CLASS_HPP
# define AWEAPON_CLASS_HPP

#include <iostream>
#include <string>

class AWeapon
{
	private:


	protected:
		std::string _name;
		int _damagePoints;
		int _actionPoints;

		AWeapon(void);

	public:
		AWeapon(std::string const & name, int apcost, int damage);
		AWeapon(AWeapon const &src);
		virtual ~AWeapon(); // deferrenced ?

		virtual void attack() const = 0;

		AWeapon &	operator=(AWeapon const &rhs);

		std::string const& getName() const;
		int getAPCost() const;
		int getDamage() const;
};

//std::ostream &	operator<<(std::ostream &o, AWeapon const &rhs);

#endif
