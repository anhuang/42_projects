#include "AWeapon.hpp"
#include "PlasmaRifle.hpp"
#include "SuperMutant.hpp"
#include "PowerFist.hpp"
#include "Enemy.hpp"
#include "RadScorpion.hpp"
#include "Character.hpp"

int main()
{
	Character* zaz = new Character("zaz");
	std::cout << *zaz;
	Enemy* a = new SuperMutant();
	Enemy* b = new RadScorpion();
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);
	zaz->attack(b);
	std::cout << *zaz;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->recoverAP();
	zaz->attack(a);
	std::cout << *zaz;
	zaz->recoverAP();
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	zaz->recoverAP();
	zaz->attack(a);
	std::cout << *zaz;
	zaz->attack(a);
	std::cout << *zaz;
	return 0;
}
