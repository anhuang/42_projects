#ifndef RADSCORPION_CLASS_HPP
# define RADSCORPION_CLASS_HPP

#include <iostream>
#include <string>
#include "Enemy.hpp"

class RadScorpion : public Enemy
{
	private:

	protected:


	public:
		RadScorpion(void);
		RadScorpion(RadScorpion const &src);
		virtual ~RadScorpion(void);
		
		virtual void takeDamage(int);

		RadScorpion &	operator=(RadScorpion const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, RadScorpion const &rhs);

#endif
