#ifndef POWERFIST_CLASS_HPP
# define POWERFIST_CLASS_HPP

#include "AWeapon.hpp"


class PowerFist : public AWeapon
{
	private:

	protected:

	public:
		PowerFist(void);
		PowerFist(PowerFist const &src);
		~PowerFist(void);

		virtual void attack() const;

		PowerFist &	operator=(PowerFist const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, PowerFist const &rhs);

#endif
