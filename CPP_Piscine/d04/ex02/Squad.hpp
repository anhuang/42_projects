#ifndef SQUAD_CLASS_HPP
# define SQUAD_CLASS_HPP

#include <iostream>
#include <string>
#include "ISquad.hpp"
#include "ISpaceMarine.hpp"

class Squad : public ISquad
{
	struct listSquad {
		ISpaceMarine* cur;
		listSquad* next;
	};

	private:
		listSquad* _listSquad;
		int _nbUnits;

	protected:

	public:
		Squad(void);
		Squad(Squad const &src);
		~Squad(void);

		virtual int getCount() const;
		virtual ISpaceMarine* getUnit(int) const;
		virtual int push(ISpaceMarine*);

		Squad &	operator=(Squad const &rhs);
};
//std::ostream &	operator<<(std::ostream &o, Squad const &rhs);

#endif
