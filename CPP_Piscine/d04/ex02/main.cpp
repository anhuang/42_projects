#include "Squad.hpp"
#include "TacticalMarine.hpp"
#include "AssaultTerminator.hpp"


int	checkLeaks( void ) {
	ISpaceMarine*	bob = new TacticalMarine;
	ISpaceMarine*	jim = new AssaultTerminator;

	ISpaceMarine*	bobbis = new TacticalMarine;
	ISpaceMarine*	jimbis = new AssaultTerminator;

	ISpaceMarine*	bar = new TacticalMarine;
	AssaultTerminator*	foo = new AssaultTerminator;
	AssaultTerminator	zug(*foo);

	Squad* vlc = new Squad;

	vlc->push(bob);
	*jim = *bob;
	vlc->push(jim);
	vlc->push(jim);
	vlc->push(NULL);

	std::cout << "New squad : first" << std::endl;
	Squad*	first = new Squad;
	first->push(bobbis);
	*jimbis = *bobbis;
	first->push(jimbis);

	std::cout << "New squad (assigned to first)" << std::endl;
	Squad*	squad = new Squad;

	*squad = *first;
	squad->push(bar);
	ISpaceMarine*	toto = squad->getUnit(42);
	if (toto)
		std::cout << "Get unit(invalid) returned a ISpaceMarine*" << std::endl;

	std::cout << "New squad (copy of squad)" << std::endl;
	Squad	copy(*squad);

	copy.push(new AssaultTerminator(zug));

	std::cout << std::endl << "Content of vlc army" << std::endl
		<< "There must be 2 units" << std::endl;
	for (int i = 0; i < vlc->getCount(); ++i)
	{
		ISpaceMarine* cur = vlc->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Content of squad army" << std::endl
		<< "There must be 3 units" << std::endl;
	for (int i = 0; i < squad->getCount(); ++i)
	{
		ISpaceMarine* cur = squad->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Content of copy army" << std::endl
		<< "There must be 4 units" << std::endl;
	for (int i = 0; i < copy.getCount(); ++i)
	{
		ISpaceMarine* cur = copy.getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}

	std::cout << std::endl << "Delete all Squads" << std::endl;
	delete vlc;
	delete squad;
	delete foo;
	delete first;
	std::cout << "End of function" << std::endl;
	return (0);
}

int main( void )
{
	checkLeaks();
//	while (1) std::cout << "";
	return 0;
}

// int main()
// {
// 	ISpaceMarine* bob = new TacticalMarine;
// 	ISpaceMarine* jim = new AssaultTerminator;
// 	ISquad* vlc = new Squad;
// 	ISquad* vlc2;
// 	vlc->push(bob);
// 	vlc->push(jim);
//
// 	vlc2 = vlc;
// 	for (int i = 0; i < vlc->getCount(); ++i) // for 0 < 2
// 	{
// 		ISpaceMarine* cur = vlc->getUnit(i);
// 		cur->battleCry();
// 		cur->rangedAttack();
// 		cur->meleeAttack();
// 	}
//
// 	std::cout << std::endl << "###COPY OF SQUAD###" << std::endl;
// 	for (int i = 0; i < vlc2->getCount(); ++i) // for 0 < 2
// 	{
// 		ISpaceMarine* cur = vlc2->getUnit(i);
// 		cur->battleCry();
// 		cur->rangedAttack();
// 		cur->meleeAttack();
// 	}
// 	delete vlc;
//
// 	std::cout << std::endl << std::endl << "My test:" << std::endl << std::endl;
//
// 	ISpaceMarine* billy = new TacticalMarine;
// 	ISpaceMarine* maelis = new AssaultTerminator;
//
// 	ISquad* vlc3 = new Squad;
// 	std::cout << "should return NULL " << vlc2->getUnit(0) << std::endl;
// 	std::cout << "should return 0 " << vlc2->getCount() << std::endl;
// 	vlc2->push(billy);
// 	vlc2->push(maelis);
// 	std::cout << "should return billy address " << vlc2->getUnit(0) << std::endl;
// 	std::cout << "should return NULL " << vlc2->getUnit(42) << std::endl;
// 	std::cout << "should return 2 " << vlc2->getCount() << std::endl;
//
// 	ISpaceMarine* boby = jim->clone();
// 	boby->battleCry();
// 	delete boby;
// 	maelis->battleCry();
//
// 	delete vlc3;
// 	return 0;
// }
