#include "AssaultTerminator.hpp"


AssaultTerminator::AssaultTerminator(void)
{
	std::cout << "* teleports from space *" << std::endl;
//	std::cout << " AssaultTerminator Default Constructor " << std::endl;
	return;
}

AssaultTerminator::AssaultTerminator(AssaultTerminator const &src)
{
	std::cout << "* teleports from space *" << std::endl;
//	std::cout << " AssaultTerminator Copy Constructor " << std::endl;
	*this = src;
	return;
}

AssaultTerminator::~AssaultTerminator(void)
{
	std::cout << "I’ll be back ..." << std::endl;
//	std::cout << " AssaultTerminator Destructor " << std::endl;
	return;
}

AssaultTerminator &	AssaultTerminator::operator=(AssaultTerminator const &rhs)
{
//	std::cout << " AssaultTerminator Assignement Operator " << std::endl;
	if (this != &rhs)
		;
	return *this;
}

ISpaceMarine* AssaultTerminator::clone() const {
	return new AssaultTerminator(*this);
}
void AssaultTerminator::battleCry() const {
	std::cout << "This code is unclean. PURIFY IT !" << std::endl;
}
void AssaultTerminator::rangedAttack() const {
	std::cout << "* does nothing *" << std::endl;
}
void AssaultTerminator::meleeAttack() const {
	std::cout << "* attacks with chainfists *" << std::endl;
}
