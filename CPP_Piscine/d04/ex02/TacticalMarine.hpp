#ifndef TACTICALMARINE_CLASS_HPP
# define TACTICALMARINE_CLASS_HPP

#include <iostream>
#include <string>
#include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine
{
	private:

	protected:

	public:
		TacticalMarine(void);
		TacticalMarine(TacticalMarine const &src);
		~TacticalMarine();

		virtual ISpaceMarine* clone() const;
		virtual void battleCry() const;
		virtual void rangedAttack() const;
		virtual void meleeAttack() const;
		TacticalMarine &	operator=(TacticalMarine const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, TacticalMarine const &rhs);

#endif
