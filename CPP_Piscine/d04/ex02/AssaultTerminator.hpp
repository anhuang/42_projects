#ifndef ASSAULTERMINATOR_CLASS_HPP
# define ASSAULTERMINATOR_CLASS_HPP

#include <iostream>
#include <string>
#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine
{
	private:

	protected:

	public:
		AssaultTerminator(void);
		AssaultTerminator(AssaultTerminator const &src);
		~AssaultTerminator(void);

		virtual ISpaceMarine* clone() const;
		virtual void battleCry() const;
		virtual void rangedAttack() const;
		virtual void meleeAttack() const;
		AssaultTerminator &	operator=(AssaultTerminator const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, AssaultTerminator const &rhs);

#endif
