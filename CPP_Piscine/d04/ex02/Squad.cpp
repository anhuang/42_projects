#include "Squad.hpp"

Squad::Squad(void) : _listSquad(NULL), _nbUnits(0)
{
//	std::cout << " Squad Default Constructor " << std::endl;
	return;
}

Squad::Squad(Squad const &src)
{
//	std::cout << " Squad Copy Constructor " << std::endl;
	*this = src;
	return;
}

Squad::~Squad(void)
{
	if (_listSquad) {
		while (_listSquad->next) {
			listSquad* temp = _listSquad;
			_listSquad = _listSquad->next;
			delete temp;
		}
		delete _listSquad->cur;
	}
//	std::cout << " Squad Destructor " << std::endl;
	return;
}

Squad &	Squad::operator=(Squad const &rhs)
{
	// std::cout << " Squad Assignement Operator " << std::endl;

	if (this != &rhs){
		this->_nbUnits = rhs.getCount();
		listSquad* copy = this->_listSquad;
		if (!copy)
			copy = new listSquad;
		for (int i = 0; i < rhs.getCount(); i++) {
			copy->cur = rhs.getUnit(i);
			copy->next = new listSquad();
			copy = copy->next;
		}
		copy->next = NULL;
	}
	return *this;
}

int Squad::getCount() const {
	return _nbUnits;
}

ISpaceMarine* Squad::getUnit(int index) const {
	if (!_listSquad)
		return NULL;
	listSquad* copy = _listSquad;
	for (int i = 0; copy->next && i < index; i++) {
		copy = copy->next;
	}
	return copy->cur;
}

int Squad::push(ISpaceMarine* newMarine) {
	if (!newMarine)
		return _nbUnits;
	if (!_listSquad)
		_listSquad = new listSquad();
	listSquad* copy = _listSquad;
	if (copy->cur == newMarine)
		return _nbUnits;
	while (copy->next) {
		if (copy->cur == newMarine)
			return _nbUnits;
		copy = copy->next;
	}
	copy->cur = newMarine;
	copy->next = new listSquad();
	copy->next = NULL;
	_nbUnits++;
	return _nbUnits;
}
