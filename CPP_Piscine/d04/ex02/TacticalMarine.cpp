#include "TacticalMarine.hpp"


TacticalMarine::TacticalMarine(void)
{
	std::cout << "Tactical Marine ready for battle" << std::endl;
//	std::cout << " TacticalMarine Default Constructor " << std::endl;
	return;
}

TacticalMarine::TacticalMarine(TacticalMarine const &src)
{
	std::cout << "Tactical Marine ready for battle" << std::endl;
//	std::cout << " TacticalMarine Copy Constructor " << std::endl;
	*this = src;
	return;
}

TacticalMarine::~TacticalMarine(void)
{
	std::cout << "Aaargh ..." << std::endl;
//	std::cout << " TacticalMarine Destructor " << std::endl;
	return;
}

TacticalMarine &	TacticalMarine::operator=(TacticalMarine const &rhs)
{
//	std::cout << " TacticalMarine Assignement Operator " << std::endl;
	if (this != &rhs)
		;
	return *this;
}

ISpaceMarine* TacticalMarine::clone() const {
	return new TacticalMarine(*this);
}
void TacticalMarine::battleCry() const {
	std::cout << "For the holy PLOT !" << std::endl;
}
void TacticalMarine::rangedAttack() const {
	std::cout << "* attacks with bolter *" << std::endl;
}
void TacticalMarine::meleeAttack() const {
	std::cout << "* attacks with chainsword *" << std::endl;
}
