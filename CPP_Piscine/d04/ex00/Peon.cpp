#include "Peon.hpp"


Peon::Peon(void)
{
	std::cout << "Zog zog." << std::endl;
//	std::cout << " Peon Default Constructor " << std::endl;
	return;
}

Peon::Peon(std::string name) : Victim(name)
{
	std::cout << "Zog zog." << std::endl;
}

Peon::Peon(Peon const &src)
{
//	std::cout << " Peon Copy Constructor " << std::endl;
	*this = src;
	return;
}

Peon::~Peon(void)
{
	std::cout << "Bleuark..." << std::endl;
//	std::cout << " Peon Destructor " << std::endl;
	return;
}

Peon &	Peon::operator=(Peon const &rhs)
{
//	std::cout << " Peon Assignement Operator " << std::endl;

	if (this != &rhs)
		this->_name = rhs.getName();

	return *this;
}
