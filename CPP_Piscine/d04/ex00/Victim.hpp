#ifndef VICTIM_CLASS_HPP
# define VICTIM_CLASS_HPP

#include <iostream>
#include <string>

class Victim
{
	private:
	protected:
		std::string _name;

	public:
		Victim(void);
		Victim(std::string);
		Victim(Victim const &src);
		~Victim(void);

		void getPolymorphed() const;
		Victim &	operator=(Victim const &rhs);

		std::string getName() const ;
};

std::ostream &	operator<<(std::ostream &o, Victim const &rhs);

#endif
