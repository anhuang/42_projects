#include "Sorcerer.hpp"
#include <stdexcept>

Sorcerer::Sorcerer(void)
{
}

Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title)
{
	std::cout << _name << ", " << _title << ", is born !" << std::endl;return;
}


Sorcerer::Sorcerer(Sorcerer const &src)
{
//	std::cout << " Sorcerer Copy Constructor " << std::endl;
	*this = src;
	return;
}

Sorcerer::~Sorcerer(void)
{
	std::cout << _name << ", " << _title << ", is dead. Consequences will never be the same !" << std::endl;
//	std::cout << " Sorcerer Destructor " << std::endl;
	return;
}

Sorcerer &	Sorcerer::operator=(Sorcerer const &rhs)
{
//	std::cout << " Sorcerer Assignement Operator " << std::endl;
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_title = rhs.getTitle();
	}
	return *this;
}

std::ostream &	operator<<(std::ostream &o, Sorcerer const &rhs)
{
	o << "I am " << rhs.getName() << ", " << rhs.getTitle() << ", and I like ponies !" << std::endl;
	return o;
}

void Sorcerer::polymorph(Victim const & rhs) const {
	rhs.getPolymorphed();
}

std::string Sorcerer::getName() const { return _name; }
std::string Sorcerer::getTitle() const { return _title; }
