#ifndef PEON_CLASS_HPP
# define PEON_CLASS_HPP

#include <iostream>
#include <string>
#include "Victim.hpp"

class Peon : public Victim
{
	private:

	protected:


	public:
		Peon(void);
		Peon(std::string);
		Peon(Peon const &src);
		~Peon(void);

		Peon &	operator=(Peon const &rhs);
};

//std::ostream &	operator<<(std::ostream &o, Peon const &rhs);

#endif
