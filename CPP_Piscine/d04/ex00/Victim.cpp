#include "Victim.hpp"

Victim::Victim(void) : _name("Default_Victim")
{
	std::cout << "Some random victim called " << _name << " just popped !" << std::endl;
//	std::cout << " Victim Default Constructor " << std::endl;
	return;
}

Victim::Victim(std::string name) : _name(name)
{
	std::cout << "Some random victim called " << _name << " just popped !" << std::endl;
//	std::cout << " Victim Default Constructor " << std::endl;
	return;
}

Victim::Victim(Victim const &src)
{
//	std::cout << " Victim Copy Constructor " << std::endl;
	*this = src;
	return;
}

Victim::~Victim(void)
{
	std::cout << "Victim " << _name << " just died for no apparent reason !" << std::endl;
//	std::cout << " Victim Destructor " << std::endl;
	return;
}

Victim &	Victim::operator=(Victim const &rhs)
{
//	std::cout << " Victim Assignement Operator " << std::endl;

	if (this != &rhs)
		this->_name = rhs.getName();

	return *this;
}

void Victim::getPolymorphed() const {
	std::cout << _name << " has been turned into a cute little sheep !" << std::endl;
}


std::ostream &	operator<<(std::ostream &o, Victim const &rhs)
{
	o << "I'm " << rhs.getName() << " and I like otters !" << std::endl;
	return o;
}

std::string Victim::getName() const { return _name; }
