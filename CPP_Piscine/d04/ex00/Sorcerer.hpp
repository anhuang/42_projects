#ifndef SORCERER_CLASS_HPP
# define SORCERER_CLASS_HPP

#include <iostream>
#include <string>
#include "Victim.hpp"

class Sorcerer
{
	private:
		std::string _name;
		std::string _title;

	protected:
		Sorcerer(void);

	public:
		Sorcerer(std::string, std::string);
		Sorcerer(Sorcerer const &src);
		~Sorcerer(void);

		Sorcerer &	operator=(Sorcerer const &rhs);
		void polymorph(Victim const &) const;
		std::string getName() const ;
		std::string getTitle() const ;
};

std::ostream &	operator<<(std::ostream &o, Sorcerer const &rhs);

#endif
