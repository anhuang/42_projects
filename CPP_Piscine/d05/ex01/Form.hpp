#ifndef FORM_CLASS_HPP
# define FORM_CLASS_HPP

#include <iostream>
#include <string>
#include <exception>

class Bureaucrat;

class Form
{


	private:
		const std::string _name;
		bool _signed;
		const int _gradeSign;
		const int _gradeExe;

		Form(void);
	protected:


	public:
		class GradeTooHighException : public std::exception
		{
			public :
				virtual const char* what() const throw();
		};

		class GradeTooLowException : public std::exception
		{
			public :
				virtual const char* what() const throw();
		};

		Form(std::string , int, int);
		Form(Form const &src);
		~Form(void);

		void beSigned(Bureaucrat & );

		Form &	operator=(Form const &rhs);
		std::string getName() const;
		bool getSigned() const;
		int getGradeSign() const;
		int getGradeExe() const;
};

std::ostream &	operator<<(std::ostream &o, Form const &rhs);

#endif
