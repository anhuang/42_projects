#include "Bureaucrat.hpp"
#include "Form.hpp"

Bureaucrat::Bureaucrat(void)
{
//	std::cout << " Bureaucrat Default Constructor " << std::endl;
	return;
}

Bureaucrat::Bureaucrat(std::string const name, int grade) : _name(name), _grade(grade)
{
//	std::cout << " Bureaucrat Default Constructor " << std::endl;
	if (_grade > 150)
		throw Bureaucrat::GradeTooLowException();
	else if (_grade < 1)
		throw Bureaucrat::GradeTooHighException();
	return;
}

Bureaucrat::Bureaucrat(Bureaucrat const &src) : _name(src.getName())
{
//	std::cout << " Bureaucrat Copy Constructor " << std::endl;
	*this = src;
	return;
}

Bureaucrat::~Bureaucrat(void)
{
//	std::cout << " Bureaucrat Destructor " << std::endl;
	return;
}

Bureaucrat &	Bureaucrat::operator=(Bureaucrat const &rhs)
{
//	std::cout << " Bureaucrat Assignement Operator " << std::endl;
	if (this != &rhs) {
		_grade = rhs.getGrade();
	}
	return *this;
}

std::ostream &	operator<<(std::ostream &o, Bureaucrat const &rhs) {
	o << rhs.getName() << ", Bureaucrat grade " << rhs.getGrade() << ".";
	return o;
}

void Bureaucrat::signForm(Form& form)
{
	if (form.getSigned())
		std::cout << _name << " signs " << form.getName() << std::endl;
	else
		std::cout << _name << " cannot sign " << form.getName() << " because " \
		<< _name << " grade are lowest "<< std::endl;
}

void Bureaucrat::incrementGrade() {
	_grade--;
	if (_grade < 1)
		throw Bureaucrat::GradeTooHighException();
}
void Bureaucrat::decrementGrade() {
	_grade++;
	if (_grade > 150)
		throw Bureaucrat::GradeTooLowException();
}

int Bureaucrat::getGrade() const { return _grade; }
std::string Bureaucrat::getName() const { return _name; }

const char* Bureaucrat::GradeTooHighException::what() const throw() {
	return "ERROR : Grade Too High(0) ! Must be between [1 - 150]";
}
const char* Bureaucrat::GradeTooLowException::what() const throw() {
	return "ERROR : Grade Too Low(151) ! Must be between [1 - 150]";
}
