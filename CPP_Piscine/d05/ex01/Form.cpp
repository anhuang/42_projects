#include "Form.hpp"
#include "Bureaucrat.hpp"

Form::Form(void) : _gradeSign(2), _gradeExe(5)
{
	return;
}

Form::Form(std::string name, int gradeSign, int gradeExe)\
: _name(name), _signed(false), _gradeSign(gradeSign), _gradeExe(gradeExe)
{
	if (_gradeSign > 150 || _gradeExe > 150)
		throw Form::GradeTooLowException();
	else if (_gradeSign < 1 || _gradeExe < 1)
		throw Form::GradeTooHighException();
	return;
}

Form::Form(Form const &src) : _name(src.getName()), _gradeSign(src.getGradeSign()), _gradeExe(src.getGradeExe())
{
	*this = src;
}

Form::~Form(void)
{
	return;
}

Form &	Form::operator=(Form const &rhs)
{
	if (this != &rhs) {
		_signed = rhs.getSigned();
	}
	return *this;
}

void Form::beSigned(Bureaucrat & bur) {
	if (bur.getGrade() <= _gradeSign) {
		_signed = true;
	}
	bur.signForm(*this);
	if (!_signed)
		throw Form::GradeTooLowException();
}

std::ostream &	operator<<(std::ostream &o, Form const &rhs) {
	o << "Name :" << rhs.getName() << std::endl \
	  << "Signed :" << rhs.getSigned() << std::endl \
	  << "Grade Require to Sign :" << rhs.getGradeSign() << std::endl \
	  << "Grade Require to Execute :" << rhs.getGradeExe();
	return o;
}

std::string Form::getName() const { return _name; }
bool Form::getSigned() const { return _signed; }
int Form::getGradeSign() const { return _gradeSign; }
int Form::getGradeExe() const { return _gradeExe; }

const char* Form::GradeTooHighException::what() const throw() {
	return "ERROR : GradeSign or/and GradeExe  Too High(0) ! Must be between [1 - 150]";
}
const char* Form::GradeTooLowException::what() const throw() {
	return "ERROR: Could no go higher than the grade required to execute it.";
}
