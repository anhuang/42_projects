#include "Bureaucrat.hpp"
#include "Form.hpp"

int main(void)
{
	try
	{
		Bureaucrat bc2("Andrew", 149);
		std::cout << bc2 << std::endl;
		bc2.decrementGrade();
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;

	try {
		Bureaucrat bc("Ernest", 2);
		std::cout << bc << std::endl;
		bc.incrementGrade();
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		std::cout << "TOO HIGH : " << std::endl;
		Bureaucrat bob = Bureaucrat("bob", 0);
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cerr << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		std::cout << "TOO LOW : " << std::endl;
		Bureaucrat bob = Bureaucrat("bob", 151);
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cerr << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		Bureaucrat bob = Bureaucrat("bob", 1);
		std::cout << "INCREMENT : " << bob << std::endl;
		bob.incrementGrade();
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cerr << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		Bureaucrat bob = Bureaucrat("bob", 150);
		std::cout << "DECREMENT : " << bob << std::endl;
		bob.decrementGrade();
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cerr << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl << "## ALL SAME ##" << std::endl;
	Bureaucrat    employee("Annoying employee", 150);
	std::cout << employee << std::endl;

	Bureaucrat copy(employee);
	std::cout << copy << std::endl;
	Bureaucrat assign = employee;
	std::cout << assign << std::endl;
	std::cout <<  "## ALL SAME ##\n" << std::endl;

// FORM TEST

	try {
		std::cout <<  "## FORM TEST ##" << std::endl;
		std::cout << "TOO HIGH : " << std::endl;
		Form form2("LeMosh", 150, 0);
	}
	catch(std::exception & e) {
		std::cerr << e.what() << std::endl;
	}

	try {
		std::cout << "TOO LOW : " << std::endl;
		Form form3("LeBoo", 151, 0);
	}
	catch(std::exception & e) {
		std::cerr << e.what() << std::endl;
	}

	try {
		std::cout << "PERFECT FORM" << std::endl;
		Form form4("LeBG", 150, 1);
	}
	catch(std::exception & e) {
		std::cerr << e.what() << std::endl;
	}
	std::cout << std::endl;

	Form form1("Benoit", 50, 100);
	Form formcopy = form1;
	Bureaucrat bur1("Bur", 50);

	std::cout << form1 << "\n == \n" << formcopy << std::endl;
	std::cout << std::endl << "CHECK SIGN : " << formcopy.getGradeSign() << " >= " << bur1.getGrade() << std::endl;
	formcopy.beSigned(bur1);
	std::cout << " SIGNED ! \n" << formcopy << "\n\n" << std::endl;

	try {
		std::cout << "ERROR SIGN :" << std::endl;
		Form formcopy2("Error Sign", 49, 2);
		formcopy2.beSigned(bur1);
	}
	catch(std::exception & e) {
		std::cerr << e.what() << std::endl;
	}
	std::cout << "## END FORM TEST ##" << std::endl;


	return 0;
}
