#ifndef BUREAUCRAT_CLASS_HPP
# define BUREAUCRAT_CLASS_HPP

#include <iostream>
#include <string>
#include <exception>

class Bureaucrat
{
	private:
		std::string const _name;
		int _grade;

		Bureaucrat(void);
	protected:

	public:
		class GradeTooHighException : public std::exception {
			public :
			virtual const char* what() const throw();
		};

		class GradeTooLowException : public std::exception {
			public :
			virtual const char* what() const throw();
		};

		Bureaucrat(std::string name, int grade);
		Bureaucrat(Bureaucrat const &src);
		~Bureaucrat(void);

		Bureaucrat &	operator=(Bureaucrat const &rhs);

		int getGrade() const;
		std::string getName() const;

		void incrementGrade();
		void decrementGrade();
};

std::ostream &	operator<<(std::ostream &o, Bureaucrat const &rhs);

#endif
