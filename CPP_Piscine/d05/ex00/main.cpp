#include "Bureaucrat.hpp"

int main(void)
{
	try
	{
		Bureaucrat bc2("Andrew", 149);
		std::cout << bc2 << std::endl;
		bc2.decrementGrade();
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;

	try {
		Bureaucrat bc("Ernest", 2);
		std::cout << bc << std::endl;
		bc.incrementGrade();
	}
	catch (std::exception & e)
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		std::cout << "TOO HIGH : " << std::endl;
		Bureaucrat bob = Bureaucrat("bob", 0);
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cout << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		std::cout << "TOO LOW : " << std::endl;
		Bureaucrat bob = Bureaucrat("bob", 151);
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cout << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		Bureaucrat bob = Bureaucrat("bob", 1);
		std::cout << "INCREMENT : " << bob << std::endl;
		bob.incrementGrade();
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cout << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl;
	try {
		Bureaucrat bob = Bureaucrat("bob", 150);
		std::cout << "DECREMENT : " << bob << std::endl;
		bob.decrementGrade();
	}
	catch(const Bureaucrat::GradeTooHighException& e) {
		std::cout << e.what() << std::endl;
	}
	catch(const Bureaucrat::GradeTooLowException& e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}
