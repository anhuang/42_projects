#ifndef ROBOTOMYREQUESTFORM_CLASS_HPP
# define ROBOTOMYREQUESTFORM_CLASS_HPP

#include <iostream>
#include <string>
#include "Form.hpp"

class RobotomyRequestForm : public Form
{
	private:
		RobotomyRequestForm(void);
		mutable bool _executed;
	protected:


	public:
		RobotomyRequestForm(std::string target);

		RobotomyRequestForm(RobotomyRequestForm const &src);
		~RobotomyRequestForm(void);

		virtual void execute(Bureaucrat const & executor) const;

		RobotomyRequestForm &	operator=(RobotomyRequestForm const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, RobotomyRequestForm const &rhs);

#endif
