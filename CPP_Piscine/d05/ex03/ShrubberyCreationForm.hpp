#ifndef SHRUBBERYCREATIONFORM_CLASS_HPP
# define SHRUBBERYCREATIONFORM_CLASS_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "Form.hpp"

class ShrubberyCreationForm : public Form
{
	private:
		ShrubberyCreationForm(void);
	protected:


	public:
		ShrubberyCreationForm(std::string target);

		ShrubberyCreationForm(ShrubberyCreationForm const &src);
		~ShrubberyCreationForm(void);

		virtual void execute(Bureaucrat const & executor) const;

		ShrubberyCreationForm &	operator=(ShrubberyCreationForm const &rhs);
};

//std::ostream &	operator<<(std::ostream &o, ShrubberyCreationForm const &rhs);

#endif
