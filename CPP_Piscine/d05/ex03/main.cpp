#include <stdexcept>

#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

// static void
// signAndExecuteForms(Bureaucrat b, ShrubberyCreationForm f1, \
//                     RobotomyRequestForm f2, PresidentialPardonForm f3)
// {
//     try
//     {
// 		std::cout << "----------" << std::endl;
// 		std::cout << b << std::endl;
// 		std::cout << f1 << std::endl;
// 		std::cout << f2 << std::endl;
// 		std::cout << f3 << std::endl;
// 		std::cout << "----------" << std::endl;
//         b.signForm(f1);
//         b.signForm(f2);
//         b.signForm(f3);
//
//         b.executeForm(f1);
//         b.executeForm(f2);
//         b.executeForm(f3);
//     }
//     catch (std::exception &e)
//     {
//         std::cout << e.what() << std::endl;
//     }
//     return ;
// }
//
// int
// main(void)
// {
//     Bureaucrat                    b1("B1", 69);
//     Bureaucrat                    b2("B2", 42);
//     Bureaucrat                    b3("B3", 1);
//     ShrubberyCreationForm         f1("SCF_2");
//     RobotomyRequestForm           f2("RRF_2");
//     PresidentialPardonForm        f3("PDF_2");
//
//     signAndExecuteForms(b1, f1, f2, f3);
//     std::cout << std::endl;
//
//     signAndExecuteForms(b2, f1, f2, f3);
//     std::cout << std::endl;
//
//     signAndExecuteForms(b3, f1, f2, f3);
//     std::cout << std::endl;
//
// 	Bureaucrat bill("bill", 72);
//  ShrubberyCreationForm shrubberyCreation("Hey");
//  RobotomyRequestForm robotomyRequest("Hey");
//  PresidentialPardonForm presidentialPardon("Hey");
//
//  std::cout << bill << std::endl;
//
//  std::cout << std::endl;
//  std::cout << "test: bill try to execute form: \"";
//  std::cout << presidentialPardon << "\""<< std::endl;
//  bill.executeForm(presidentialPardon);
//
//  std::cout << std::endl;
//  std::cout << "test: bill try to sign form: \"";
//  std::cout << robotomyRequest << "\""<< std::endl;
//  bill.signForm(robotomyRequest);
//
//  std::cout << std::endl;
//  std::cout << "test: bill try to sign form: \"";
//  std::cout << presidentialPardon << "\""<< std::endl;
//  bill.signForm(presidentialPardon);
//
//  std::cout << std::endl;
//  std::cout << "test: bill try to execute form: \"";
//  std::cout << presidentialPardon << "\""<< std::endl;
//  bill.executeForm(presidentialPardon);
//
//  std::cout << std::endl;
//  bill.signForm(shrubberyCreation);
//
//  std::cout << std::endl;
//  std::cout << "test: bill try to execute form: \"";
//  std::cout << shrubberyCreation << "\""<< std::endl;
//  bill.executeForm(shrubberyCreation);
//
//  std::cout << std::endl;
//  std::cout << "create new burocrate: \"";
//  Bureaucrat bob("bob", 1);
//  std::cout << bob << "\""<< std::endl;
//
//  std::cout << std::endl;
//  std::cout << "test: bob try to execute form: \"";
//  std::cout << presidentialPardon << "\""<< std::endl;
//  bob.executeForm(presidentialPardon);
//
//  std::cout << std::endl;
//  bob.signForm(robotomyRequest);
//  std::cout << "test: bob try to execute form: \"";
//  std::cout << robotomyRequest << "\""<< std::endl;
//  bob.executeForm(robotomyRequest);
//
// return (0);
// }

void signAndExecute(Bureaucrat & b, Form & f)
{
	try {
		std::cout << b << std::endl;
		std::cout << f << std::endl;
		b.signForm(f);
		b.executeForm(f);
	}
    catch (std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}

int main(void)
{
	Intern someRandomIntern;
	Form* rrf;
	Bureaucrat                    b1("B1", 45);
	Bureaucrat                    b2("B2", 5);
	Bureaucrat                    b3("B3", 50);

	rrf = someRandomIntern.makeForm("robotomy request", "Bender");
	if (rrf)
		signAndExecute(b1, *rrf);

	std::cout << '\n' << std::endl;

	rrf = someRandomIntern.makeForm("     Presidential    Pardon", "Trump");
	if (rrf)
		signAndExecute(b2, *rrf);

		std::cout << '\n' << std::endl;

	rrf = someRandomIntern.makeForm("  Sh    r u b  be r y C r  e a t io n     ", "Bushh");
	if (rrf)
		signAndExecute(b3, *rrf);

	std::cout << "\n### FAIL ###\n" << std::endl;

	rrf = someRandomIntern.makeForm("r o  b  o t o m y r   e q   ue   s t", "Bushh");
	if (rrf)
		signAndExecute(b3, *rrf);

	return (0);
}
