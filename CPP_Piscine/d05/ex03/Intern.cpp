#include "Intern.hpp"

Intern::Intern(void)
{
//	std::cout << " Intern Default Constructor " << std::endl;
	return;
}

Intern::Intern(Intern const &src)
{
//	std::cout << " Intern Copy Constructor " << std::endl;
	*this = src;
	return;
}

Intern::~Intern(void)
{
//	std::cout << " Intern Destructor " << std::endl;
	return;
}

Intern &	Intern::operator=(Intern const &rhs)
{
//	std::cout << " Intern Assignement Operator " << std::endl;

	if (this != &rhs)
		;
//		this->_foo = rhs.getFoo();

	return *this;
}

// typedef void (Form::*tabPtr)(std::string target);

// struct arrayForm{
// 	Form fn;
// 	const std::string key;
// };

Form* Intern::makeForm(std::string nameForm, std::string target)
{
	// arrayForm tableOfFunction[] =
	// 	{
	// 		{ &RobotomyRequestForm::RobotomyRequestForm(std::string), "robotomyrequest"},
	// 		{ &PresidentialPardonForm::PresidentialPardonForm(std::string), "presidentialpardon"},
	// 		{ &ShrubberyCreationForm::ShrubberyCreationForm(std::string), "shrubberycreation"},
	// 	};
	std::string copy = nameForm;
	removeSpaces(copy);
	if (!copy.compare("robotomyrequest")) {
		RobotomyRequestForm* ret = new RobotomyRequestForm(target);
		std::cout << "Intern creates " << nameForm << std::endl;
		return ret;
	}
	else if (!copy.compare("presidentialpardon")) {
		PresidentialPardonForm* ret = new PresidentialPardonForm(target);
		std::cout << "Intern creates " << nameForm << std::endl;
		return ret;
	}
	else if (!copy.compare("shrubberycreation")) {
		ShrubberyCreationForm* ret = new ShrubberyCreationForm(target);
		std::cout << "Intern creates " << nameForm << std::endl;
		return ret;
	}
	else {
		std::cout << nameForm << " not recognized." << std::endl;
		return NULL;
	}
}

void Intern::removeSpaces(std::string & str)
{
    int count = 0;

    for (int i = 0; str[i]; i++)
        if (!isspace(str[i]))
            str[count++] = tolower(str[i]);
	str = str.substr(0, count);
}
