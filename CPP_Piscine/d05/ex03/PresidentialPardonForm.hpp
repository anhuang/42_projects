#ifndef PRESIDENTIALPARDONFORM_CLASS_HPP
# define PRESIDENTIALPARDONFORM_CLASS_HPP

#include <iostream>
#include <string>
#include "Form.hpp"

class PresidentialPardonForm : public Form
{
	private:
		PresidentialPardonForm(void);
	protected:


	public:
		PresidentialPardonForm(std::string target);

		PresidentialPardonForm(PresidentialPardonForm const &src);
		~PresidentialPardonForm(void);

		virtual void execute(Bureaucrat const & executor) const;

		PresidentialPardonForm &	operator=(PresidentialPardonForm const &rhs);

};

//std::ostream &	operator<<(std::ostream &o, PresidentialPardonForm const &rhs);

#endif
