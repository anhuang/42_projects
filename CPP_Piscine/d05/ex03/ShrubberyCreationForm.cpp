#include "ShrubberyCreationForm.hpp"
#include "Bureaucrat.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(void)
{
//	std::cout << " ShrubberyCreationForm Default Constructor " << std::endl;
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form(target, 145, 137)
{
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &src)
{
//	std::cout << " ShrubberyCreationForm Copy Constructor " << std::endl;
	*this = src;
	return;
}

ShrubberyCreationForm::~ShrubberyCreationForm(void)
{
//	std::cout << " ShrubberyCreationForm Destructor " << std::endl;
	return;
}

ShrubberyCreationForm &	ShrubberyCreationForm::operator=(ShrubberyCreationForm const &rhs)
{
//	std::cout << " ShrubberyCreationForm Assignement Operator " << std::endl;

	if (this != &rhs)
		;
//		this->_foo = rhs.getFoo();

	return *this;
}

void ShrubberyCreationForm::execute(Bureaucrat const & executor) const {
	if (executor.getGrade() > getGradeExe()){
		throw Form::GradeTooLowException();
	}
	else if (!getSigned())
		throw Form::GradeNotSigned();
	std::ofstream ofs;
	ofs.open(getName(), std::ofstream::out | std::ofstream::app);
	ofs << "\
			ccee88oo\n \
		  C8O8O8Q8PoOb o8oo\n \
		 dOB69QO8PdUOpugoO9bD\n \
		CgggbU8OU qOp qOdoUOdcb\n \
		    6OuU  /p u gcoUodpP\n \
		      \\\\//  /douUP\n \
		        \\\\////\n \
		         |||/\\\n \
		         |||\\/\\\n \
		         |||||\n \
		   .....//||||\\....\n \
		   David Moore" << std::endl;
	ofs.close();
}
