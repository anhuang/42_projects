#ifndef INTERN_HPP
# define INTERN_HPP

#include <iostream>
#include <string>
#include <ctype.h>
#include "Form.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "ShrubberyCreationForm.hpp"

class Intern
{
	private:

	protected:


	public:
		Intern(void);
		Intern(Intern const &src);
		~Intern(void);

		Intern &	operator=(Intern const &rhs);
		Form* makeForm(std::string, std::string);
		void removeSpaces(std::string & str);
};

//std::ostream &	operator<<(std::ostream &o, Intern const &rhs);

#endif
