#include "RobotomyRequestForm.hpp"
#include "Bureaucrat.hpp"

RobotomyRequestForm::RobotomyRequestForm(void)
{
//	std::cout << " RobotomyRequestForm Default Constructor " << std::endl;
	return;
}

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form(target, 72, 45), _executed(false)
{
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &src)
{
//	std::cout << " RobotomyRequestForm Copy Constructor " << std::endl;
	*this = src;
	return;
}

RobotomyRequestForm::~RobotomyRequestForm(void)
{
//	std::cout << " RobotomyRequestForm Destructor " << std::endl;
	return;
}

RobotomyRequestForm &	RobotomyRequestForm::operator=(RobotomyRequestForm const &rhs)
{
//	std::cout << " RobotomyRequestForm Assignement Operator " << std::endl;

	if (this != &rhs)
		;
//		this->_foo = rhs.getFoo();

	return *this;
}

void RobotomyRequestForm::execute(Bureaucrat const & executor) const {
	if (executor.getGrade() > getGradeExe()){
		throw Form::GradeTooLowException();
	}
	else if (!getSigned())
		throw Form::GradeNotSigned();
	std::cout << "**DRRRDRRRRRRDRRRR**" << std::endl;
	if (!_executed) {
		_executed = true;
		std::cout << getName() << " has been robotomized successfully 50% of the time." << std::endl;
	}
	else
		std::cout << "it’s a failure." << std::endl;
}
