#include "PresidentialPardonForm.hpp"
#include "Bureaucrat.hpp"

PresidentialPardonForm::PresidentialPardonForm(void)
{
//	std::cout << " PresidentialPardonForm Default Constructor " << std::endl;
	return;
}

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form(target, 25, 5)
{
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &src)
{
//	std::cout << " PresidentialPardonForm Copy Constructor " << std::endl;
	*this = src;
	return;
}

PresidentialPardonForm::~PresidentialPardonForm(void)
{
//	std::cout << " PresidentialPardonForm Destructor " << std::endl;
	return;
}

PresidentialPardonForm &	PresidentialPardonForm::operator=(PresidentialPardonForm const &rhs)
{
//	std::cout << " PresidentialPardonForm Assignement Operator " << std::endl;

	if (this != &rhs)
		;
//		this->_foo = rhs.getFoo();

	return *this;
}

void PresidentialPardonForm::execute(Bureaucrat const & executor) const {
	if (executor.getGrade() > getGradeExe()){
		throw Form::GradeTooLowException();
	}
	else if (!getSigned())
		throw Form::GradeNotSigned();
	std::cout << getName() << " has been pardoned by Zaphod Beeblebrox." << std::endl;
}
