#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
int main() {
    int i = 5;
    if (i < 0)
        return 1;
    char newFile[50];
    char exe[50];
    char compile[50];
    char *code = "#include <stdio.h>%1$c#include <stdlib.h>%1$c#include <string.h>%1$c#include <unistd.h>%1$cint main() {%1$c    int i = %4$d;%1$c    if (i < 0)%1$c        return 1;%1$c    char newFile[50];%1$c    char exe[50];%1$c    char compile[50];%1$c    char *code = %3$c%2$s%3$c;%1$c    if (access(%3$cSully_5.c%3$c, F_OK) != -1){--i;};%1$c    sprintf(newFile, %3$cSully_%%d.c%3$c, i);%1$c    FILE *fd = fopen(newFile, %3$cw%3$c);%1$c    fprintf(fd,code,10,code,34,i);%1$c    fclose(fd);%1$c    if (i < 1)%1$c        return 1;%1$c    sprintf(exe, %3$cSully_%%d%3$c, i);%1$c    sprintf(newFile, %3$cSully_%%d.c%3$c, i);%1$c    sprintf(compile, %3$ccc -o %%s %%s%3$c,exe,newFile);%1$c    system(compile);%1$c    bzero(compile,50);%1$c    sprintf(compile,%3$c./%%s%3$c,exe);%1$c    system(compile);%1$c    return 0;%1$c}";
    if (access("Sully_5.c", F_OK) != -1){--i;};
    sprintf(newFile, "Sully_%d.c", i);
    FILE *fd = fopen(newFile, "w");
    fprintf(fd,code,10,code,34,i);
    fclose(fd);
    if (i < 1)
        return 1;
    sprintf(exe, "Sully_%d", i);
    sprintf(newFile, "Sully_%d.c", i);
    sprintf(compile, "cc -o %s %s",exe,newFile);
    system(compile);
    bzero(compile,50);
    sprintf(compile,"./%s",exe);
    system(compile);
    return 0;
}